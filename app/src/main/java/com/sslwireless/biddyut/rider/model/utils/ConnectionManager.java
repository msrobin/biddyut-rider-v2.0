package com.sslwireless.biddyut.rider.model.utils;

/**
 * Created by Asif Malik on 7/24/2017.
 */

public class ConnectionManager {
    //demo
    //public static final String BASE_URL = "http://biddyut.sslwireless.com/api/v2/rider/";
    public static final String BASE_URL = "http://biddyut.publicdemo.xyz/api/v2/rider/";

    //live
    //public static final String BASE_URL = "http://system.biddyut.com/api/v2/rider/";
    //public static final String BASE_URL = "http://system.biddyut.com/api/v1/";
}
