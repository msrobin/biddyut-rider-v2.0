package com.sslwireless.biddyut.rider.view.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.ListFragBinding;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;
import com.sslwireless.biddyut.rider.model.response.task.Consignment;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.activity.ProductDetails;
import com.sslwireless.biddyut.rider.view.activity.TaskDetailsActivity;
import com.sslwireless.biddyut.rider.view.adapter.TaskListAdapter;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskListFragment extends Fragment implements TaskListAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ListFragBinding binding;
    private Context context;
    private Bundle bundle;
    private TaskList taskList;
    private TaskListAdapter adapter;
    private String apiToken;
    private ApiInterface apiInterface;

    public TaskListFragment() {
    }

    public static TaskListFragment newInstance() {
        TaskListFragment fragment = new TaskListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*bundle = getArguments();
        tasks = (TaskList) bundle.getSerializable("task_list");
        Log.d("TAG", "Task list : " + tasks);*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
        View view = binding.getRoot();

        context = getActivity().getApplicationContext();
        getApiToken(context);

        getTaskList();

        if (binding.swipeTaskList != null) {
            binding.swipeTaskList.setOnRefreshListener(this);
        }

        return view;
    }

    private void getTaskList() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskList> call = apiInterface.getTaskList(apiToken);
        call.enqueue(new Callback<TaskList>() {
            @Override
            public void onResponse(Call<TaskList> call, Response<TaskList> response) {
                taskList = response.body();
                if (response.isSuccessful()) {
                    Log.d("TAG", "status code: " + taskList.getStatusCode());
                    if (taskList.getStatusCode() == 200) {
                        if (taskList.getResponse().getConsignment() != null) {
                            saveConsignmentId(taskList.getResponse().getConsignment().getConsignmentUniqueId(), context);
                        } else {
                            saveConsignmentId("null", context);
                        }
                        binding.swipeTaskList.setRefreshing(false);
                        binding.listRecycler.setHasFixedSize(true);
                        adapter = new TaskListAdapter(context, taskList.getResponse().getTasks(), taskList);
                        binding.listRecycler.setLayoutManager(new LinearLayoutManager(context));
                        binding.listRecycler.setAdapter(adapter);
                        adapter.setClickListener(TaskListFragment.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskList> call, Throwable t) {
                binding.swipeTaskList.setRefreshing(false);
                Log.d("TAG", "Failed message: " + t.toString());
            }
        });

        /*apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getTaskList1(apiToken);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.d("TAG", "response data from server: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.d("TAG", "response data from server: " + response.code());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG", "Failed data: " + t.toString());
            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        context = getActivity().getApplicationContext();
        getApiToken(context);
        getTaskList();
    }

    @Override
    public void itemClicked(View v, int position) {
        Intent intent = new Intent(context, TaskDetailsActivity.class);
        intent.putExtra("token", apiToken);
        intent.putExtra("consignment_unique_id", taskList.getResponse().getTasks().get(position).getConsignment_unique_id());
        intent.putExtra("task_group_id", taskList.getResponse().getTasks().get(position).getTaskGroupId());
        intent.putExtra("consignment_type", taskList.getResponse().getTasks().get(position).getTask_type());
        intent.putExtra("task_list", taskList);
        startActivity(intent);
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    @Override
    public void onRefresh() {
        binding.swipeTaskList.setColorSchemeResources(R.color.colorPrimary);
        getTaskList();
    }

    private void saveConsignmentId(String loginStatus, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("consign_id", loginStatus);
        editor.commit();
    }
}
