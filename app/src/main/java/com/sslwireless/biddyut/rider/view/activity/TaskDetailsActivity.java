package com.sslwireless.biddyut.rider.view.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.TaskDetailsBinding;
import com.sslwireless.biddyut.rider.model.response.details.Product;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.adapter.TaskDetailsProductRecycler;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asif.malik on 7/27/2017.
 */

public class TaskDetailsActivity extends AppCompatActivity {

    private TaskDetailsBinding taskDetailsBinding;
    private String consignment_id, token, consignment_type;
    private int task_group_id, totalQuantity = 0;
    private Toolbar toolbar;
    private Context context;
    private ProgressDialog dialogs;
    private ApiInterface apiInterface;
    private TaskDetails taskDetails;
    private TaskDetailsProductRecycler adapter;
    private Typeface roboto_condence;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private TaskList tasks;
    private String checkUserType, number;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        taskDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_task_details);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.task_details_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");

        toolbar = (Toolbar) findViewById(R.id.task_details_app_bar);
        setSupportActionBar(toolbar);

        token = getIntent().getStringExtra("token");
        consignment_id = getIntent().getStringExtra("consignment_unique_id");
        consignment_type = getIntent().getStringExtra("consignment_type");
        task_group_id = getIntent().getIntExtra("task_group_id", 0);
        tasks = (TaskList) getIntent().getSerializableExtra("task_list");

        Log.d("TAG", "token: " + token);
        Log.d("TAG", "consignment_id: " + consignment_id);
        Log.d("TAG", "task_group_id: " + task_group_id);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressD();
        taskDetails = new TaskDetails();
        getTaskDetails();
        initTypeface();

        taskDetailsBinding.merchantPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToMobile("merchant_phone");
            }
        });

        taskDetailsBinding.customerPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToMobile("customer_phone");
            }
        });

        taskDetailsBinding.pickupPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToMobile("pickup_phone");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            /*default:
                return super.onOptionsItemSelected(item);*/
        }
        return super.onOptionsItemSelected(item);
    }

    private void initTypeface() {
        //taskDetailsBinding.taskID.setTypeface(roboto_condence);
        taskDetailsBinding.typeText.setTypeface(roboto_condence);
        taskDetailsBinding.type.setTypeface(roboto_condence);
        taskDetailsBinding.cusInformation.setTypeface(roboto_condence);
        taskDetailsBinding.name.setTypeface(roboto_condence);
        taskDetailsBinding.customerName.setTypeface(roboto_condence);
        taskDetailsBinding.email.setTypeface(roboto_condence);
        taskDetailsBinding.customerEmail.setTypeface(roboto_condence);
        taskDetailsBinding.phnNumber.setTypeface(roboto_condence);
        taskDetailsBinding.customerPhone.setTypeface(roboto_condence);
        taskDetailsBinding.merInformation.setTypeface(roboto_condence);
        taskDetailsBinding.orderId.setTypeface(roboto_condence);
        taskDetailsBinding.merchantOrderId.setTypeface(roboto_condence);
        taskDetailsBinding.merName.setTypeface(roboto_condence);
        taskDetailsBinding.merchantName.setTypeface(roboto_condence);
        taskDetailsBinding.merEmail.setTypeface(roboto_condence);
        taskDetailsBinding.merchantEmail.setTypeface(roboto_condence);
        taskDetailsBinding.phnNum.setTypeface(roboto_condence);
        taskDetailsBinding.merchantPhone.setTypeface(roboto_condence);
        taskDetailsBinding.address.setTypeface(roboto_condence);
        taskDetailsBinding.addressName.setTypeface(roboto_condence);
        taskDetailsBinding.pickUpWarehouse.setTypeface(roboto_condence);
        taskDetailsBinding.addressPhn.setTypeface(roboto_condence);
        taskDetailsBinding.pickupPhoneNumber.setTypeface(roboto_condence);
        taskDetailsBinding.addAddress.setTypeface(roboto_condence);
        taskDetailsBinding.pickupAddress.setTypeface(roboto_condence);
        taskDetailsBinding.product.setTypeface(roboto_condence);
        taskDetailsBinding.altMerchantPhone.setTypeface(roboto_condence);
        taskDetailsBinding.altPhnNum.setTypeface(roboto_condence);
        taskDetailsBinding.receivedValue.setTypeface(roboto_condence);
        taskDetailsBinding.receivedText.setTypeface(roboto_condence);
        taskDetailsBinding.quantity.setTypeface(roboto_condence);
        taskDetailsBinding.totalProductNumber.setTypeface(roboto_condence);
        taskDetailsBinding.totalProductNumberValue.setTypeface(roboto_condence);
        taskDetailsBinding.totalProductQuantity.setTypeface(roboto_condence);
        taskDetailsBinding.totalProductQuantityValue.setTypeface(roboto_condence);
    }

    private void getTaskDetails() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskDetails> call = apiInterface.getTaskDetails(token, consignment_id, String.valueOf(task_group_id));
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                if (response.isSuccessful()) {
                    dialogs.dismiss();
                    taskDetails = response.body();

                    if (taskDetails.getStatusCode() == 200) {
                        taskDetailsBinding.type.setText(consignment_type);
                        taskDetailsBinding.setTaskDetailsData(taskDetails);

                        if (taskDetails.getResponse().getProducts().get(0).getTaskType().equals("delivery")) {
                            taskDetailsBinding.totalReceivablePriceLayout.setVisibility(View.VISIBLE);
                            taskDetailsBinding.receivedValue.setText(taskDetails.getResponse().getProducts().get(0).getTotalPayableAmount() + " BDT");
                        }

                        taskDetailsBinding.totalProductNumberValue.setText(String.valueOf(taskDetails.getResponse().getProducts().size()));

                        for (int i = 0; i < taskDetails.getResponse().getProducts().size(); i++) {
                            totalQuantity = totalQuantity + taskDetails.getResponse().getProducts().get(i).getQuantity();
                        }

                        taskDetailsBinding.totalProductQuantityValue.setText(String.valueOf(totalQuantity));

                        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                        linearLayoutManager.setAutoMeasureEnabled(true);
                        taskDetailsBinding.multipleProductList.setHasFixedSize(true);
                        adapter = new TaskDetailsProductRecycler(context, taskDetails.getResponse().getProducts());
                        taskDetailsBinding.multipleProductList.setLayoutManager(linearLayoutManager);
                        taskDetailsBinding.multipleProductList.setNestedScrollingEnabled(false);
                        taskDetailsBinding.multipleProductList.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {
                dialogs.dismiss();
                Log.d("TAG", "Details failed message: " + t.toString());
            }
        });
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }

    public void callToMobile(String type) {
        checkUserType = type;
        Log.d("TAG", "check data is: " + checkUserType);
        try {
            if (checkPermission()) {
                callBrand();

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermission();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void callBrand() {
        if (checkUserType.equals("merchant_phone")) {
            number = "tel:" + taskDetailsBinding.merchantPhone.getText().toString();
            callToNumber(number);
        }
        if (checkUserType.equals("customer_phone")) {
            number = "tel:" + taskDetailsBinding.customerPhone.getText().toString();
            callToNumber(number);
        }
        if (checkUserType.equals("pickup_phone")) {
            number = "tel:" + taskDetailsBinding.customerPhone.getText().toString();
            callToNumber(number);
        }
    }

    private void callToNumber(String number) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(number));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        context.startActivity(callIntent);
    }


    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CALL_PHONE)) {
            Toast.makeText(context, "Call Phone permission allows us to call a user. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);
            }
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    callBrand();
                } else {
                    Toast.makeText(context, "You must granted phone call dialer permission", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
}
