package com.sslwireless.biddyut.rider.lib;

/**
 * Created by SSL_ZAHID on 8/21/2016.
 */
public class LibStaticData {

    /*Normal Request*/
    public static final int IMAGE_CAPTURE_PERMISSION = 300;
    public static final int GALLERY_IMAGE_REQUEST = 301;
    public static final int WRITE_EXTERNAL_STORAGE = 302;
    public static final int WRITE_EXTERNAL_STORAGE_FOR_GALLERY = 303;

    /*Run Time Permission code*/
    public static final int CAMERA_REQUEST = 400;

}
