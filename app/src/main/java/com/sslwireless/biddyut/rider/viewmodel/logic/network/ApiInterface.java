package com.sslwireless.biddyut.rider.viewmodel.logic.network;

import com.sslwireless.biddyut.rider.model.basic.BasicResponse;
import com.sslwireless.biddyut.rider.model.response.consignments.ConsignmentsModel;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;
import com.sslwireless.biddyut.rider.model.response.login.LoginModel;
import com.sslwireless.biddyut.rider.model.response.reason.StatusReason;
import com.sslwireless.biddyut.rider.model.response.reconcilation.Reconcilation;
import com.sslwireless.biddyut.rider.model.response.stask.StartTask;
import com.sslwireless.biddyut.rider.model.response.stats.StatisticsModel;
import com.sslwireless.biddyut.rider.model.response.task.Response;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.model.response.verify_address_model.VerifyAddressModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Asif Malik on 7/23/2017.
 */

public interface ApiInterface {
    @Multipart
    @POST("login")
    Call<LoginModel> userLogin(
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("key") RequestBody key
    );

    @FormUrlEncoded
    @POST("reasons")
    Call<StatusReason> statusReason(
            @Field("api_token") String api_token,
            @Field("reason_type") String reason_type
    );

    @FormUrlEncoded
    @POST("location/verify")
    Call<VerifyAddressModel> verifyAddress(
            @Field("api_token") String api_token,
            @Field("consignment_unique_id") String consignment_unique_id,
            @Field("task_group_id") String task_group_id,
            @Field("lat") String lat,
            @Field("lon") String lon
    );

    @FormUrlEncoded
    @POST("location")
    Call<BasicResponse> sendRiderData(
            @Field("api_token") String api_token,
            @Field("consignment_unique_id") String consignment_unique_id,
            @Field("lat") String lat,
            @Field("lon") String lon
    );

    /*@FormUrlEncoded
    @POST("location")
    Call<ResponseBody> verifyAddress(
            @Field("api_token") String api_token,
            @Field("consignment_unique_id") String consignment_unique_id,
            @Field("task_group_id") String task_group_id,
            @Field("lat") String lat,
            @Field("lon") String lon
    );*/

    @FormUrlEncoded
    @POST("tasks")
    Call<TaskList> getTaskList(
            @Field("api_token") String api_token
    );

    @FormUrlEncoded
    @POST("tasks")
    Call<ResponseBody> getTaskList1(
            @Field("api_token") String api_token
    );

    @FormUrlEncoded
    @POST("task")
    Call<TaskDetails> getTaskDetails(
            @Field("api_token") String api_token,
            @Field("consignment_unique_id") String consignment_unique_id,
            @Field("task_group_id") String task_group_id
    );

    @FormUrlEncoded
    @POST("task/start")
    Call<StartTask> startTask(
            @Field("api_token") String api_token,
            @Field("consignment_unique_id") String consignment_unique_id,
            @Field("task_group_id") String task_group_id,
            @Field("start_lat") String start_lat,
            @Field("start_lon") String start_lon
    );

    @FormUrlEncoded
    @POST("tasks/reconciliation")
    Call<Reconcilation> reconRequest(
            @Field("api_token") String api_token,
            @Field("consignment_unique_id") String consignment_unique_id
    );

    @Multipart
    @POST("task/submit")
    Call<StatisticsModel> sendSubmitTask(
            @Part("api_token") RequestBody api_token,
            @Part("consignment_unique_id") RequestBody consignment_unique_id,
            @Part("task_group_id") RequestBody task_group_id,
            @Part("end_lat") RequestBody end_lat,
            @Part("end_lon") RequestBody end_lon,
            @Part("distance") RequestBody distance,
            @Part("products") RequestBody products,
            @Part MultipartBody.Part pictureBody,
            @Part MultipartBody.Part signatureBody
    );

    /*@Multipart
    @POST("task/submit")
    Call<ResponseBody> sendSubmitTask(
            @Part("api_token") RequestBody api_token,
            @Part("consignment_unique_id") RequestBody consignment_unique_id,
            @Part("task_group_id") RequestBody task_group_id,
            @Part("end_lat") RequestBody end_lat,
            @Part("end_lon") RequestBody end_lon,
            @Part("distance") RequestBody distance,
            @Part("products") RequestBody products,
            @Part MultipartBody.Part pictureBody,
            @Part MultipartBody.Part signatureBody
    );*/

    @FormUrlEncoded
    @POST("consignments")
    Call<ConsignmentsModel> consignments(
            @Field("api_token") String api_token
    );
}
