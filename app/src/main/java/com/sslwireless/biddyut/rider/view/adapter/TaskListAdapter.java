package com.sslwireless.biddyut.rider.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.activity.StartedTaskActivity;

import java.util.ArrayList;

/**
 * Created by asif.malik on 7/25/2017.
 */

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Task> mTaskInfo;
    private TaskList tasks;
    private Context mContext;
    private ClickListener clickListener;
    private int task_flag = 0;
    private Typeface roboto_condence;

    public TaskListAdapter(Context context, ArrayList<Task> data, TaskList taskList) {
        inflater = LayoutInflater.from(context);
        this.mTaskInfo = data;
        this.tasks = taskList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.main_home_custom_recycler, parent, false);
        MyViewHolder holder = new MyViewHolder(view, viewType);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Task task = mTaskInfo.get(position);

        holder.task_counter.setText(String.valueOf(position + 1));
        holder.task_title.setText(task.getTaskTitle());
        holder.task_condition.setText(task.getTask_type());
        holder.task_address.setText(task.getAddress());

        if (task.getTaskStatus().equals("Pending")) {
            holder.task_status_layout.setBackgroundColor(mContext.getResources().getColor(R.color.main_home_footer_color));
            holder.task_status.setText("Start");
        } else if (task.getTaskStatus().equals("Processing")) {
            task_flag = 1;
            holder.task_status.setText("In Transit");
        } else if (task.getTaskStatus().equals("Done")) {
            holder.task_status_layout.setBackgroundColor(mContext.getResources().getColor(R.color.task_done_color));
            holder.task_status.setText("Done");
        } else if (task.getTaskStatus().equals("Completed")) {
            holder.sec_main_lay.setBackgroundColor(mContext.getResources().getColor(R.color.main_lay_color));
            holder.main_root_lay.setBackgroundColor(mContext.getResources().getColor(R.color.main_lay_color));
            holder.task_status_layout.setBackgroundColor(mContext.getResources().getColor(R.color.main_lay_color));
            holder.task_status.setText("Completed");
        }

        holder.task_status_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.task_status.getText().toString().equals("In Transit")) {
                    Intent intent = new Intent(inflater.getContext(), StartedTaskActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("task_list", tasks);
                    intent.putExtra("task_title", task.getTaskTitle());
                    intent.putExtra("task_group_id", task.getTaskGroupId());
                    inflater.getContext().startActivity(intent);
                } else if (task.getTaskStatus().equals("Done")) {
                    Toast.makeText(inflater.getContext(), "This task is already done", Toast.LENGTH_LONG).show();
                } else if (task.getTaskStatus().equals("Completed")) {
                    Toast.makeText(inflater.getContext(), "This task is already completed", Toast.LENGTH_LONG).show();
                } else if (task_flag == 1) {
                    Toast.makeText(inflater.getContext(), "You can not start another task. Please complete pending task", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(inflater.getContext(), StartedTaskActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("task_list", tasks);
                    intent.putExtra("task_title", task.getTaskTitle());
                    intent.putExtra("task_group_id", task.getTaskGroupId());
                    inflater.getContext().startActivity(intent);
                }
            }
        });
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return mTaskInfo.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView task_counter, task_title, task_condition, task_status,task_address;
        LinearLayout task_status_layout,main_root_lay,sec_main_lay;

        public MyViewHolder(View itemView, int viewType) {
            super(itemView);
            itemView.setOnClickListener(this);
            roboto_condence = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/RobotoCondensed-Bold.ttf");
            task_counter = (TextView) itemView.findViewById(R.id.task_counter);
            task_title = (TextView) itemView.findViewById(R.id.task_title);
            task_condition = (TextView) itemView.findViewById(R.id.task_condition);
            task_status = (TextView) itemView.findViewById(R.id.task_status);
            task_address = (TextView) itemView.findViewById(R.id.task_address);
            task_status_layout = (LinearLayout) itemView.findViewById(R.id.task_status_layout);
            main_root_lay = (LinearLayout) itemView.findViewById(R.id.main_root_lay);
            sec_main_lay = (LinearLayout) itemView.findViewById(R.id.sec_main_lay);

            task_counter.setTypeface(roboto_condence);
            task_title.setTypeface(roboto_condence);
            task_condition.setTypeface(roboto_condence);
            task_status.setTypeface(roboto_condence);
            task_address.setTypeface(roboto_condence);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.itemClicked(v, getPosition());
            }
        }
    }

    public interface ClickListener {
        public void itemClicked(View v, int position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*@Override
    public long getItemId(int position) {
        return position;
    }*/
}
