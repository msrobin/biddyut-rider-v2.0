package com.sslwireless.biddyut.rider.view.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.StartedTaskBinding;
import com.sslwireless.biddyut.rider.databinding.VerifyAddressBinding;
import com.sslwireless.biddyut.rider.model.basic.SubmitModel;
import com.sslwireless.biddyut.rider.model.response.stask.StartTask;
import com.sslwireless.biddyut.rider.model.response.stats.StatisticsModel;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.model.response.verify_address_model.VerifyAddressModel;
import com.sslwireless.biddyut.rider.model.utils.GoogleDirection;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import org.w3c.dom.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asif.malik on 8/28/2017.
 */

public class VerifyAddressActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener {
    private VerifyAddressBinding verifyAddressBinding;
    private TaskList taskList;
    private LatLng finalLatLong = null;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private Context context;
    private List<Marker> mMarkers = new ArrayList<Marker>();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private int flag = 0, gpsCounter = 0, timecounter = 0;
    private LatLng loc, oldLoc;
    private float currentZoom = -1;
    private String title, task_title, apiToken;
    private ApiInterface apiInterface;
    private double submittedLat, submittedLng;
    private VerifyAddressModel verifyAddressModel;
    private Toolbar toolbar;
    private Typeface roboto_condence;
    private StatisticsModel statisticsModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        verifyAddressBinding = DataBindingUtil.setContentView(this, R.layout.activity_verify_address);
        context = this;
        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");

        toolbar = (Toolbar) findViewById(R.id.add_app_bar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getApiToken(context);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.verify_address_map);
        mapFragment.getMapAsync(this);
        taskList = (TaskList) getIntent().getSerializableExtra("task_list");
        statisticsModel = (StatisticsModel) getIntent().getSerializableExtra("stats");

        verifyAddressBinding.verifyAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalLatLong != null) {
                    submittedLat = finalLatLong.latitude;
                    submittedLng = finalLatLong.longitude;
                    verifyAddressBinding.verifyAddress.setEnabled(false);
                    sendVerifyAddress();
                } else {
                    Toast.makeText(context, "Press and hold on the map to get the current location", Toast.LENGTH_LONG).show();
                }
            }
        });

        verifyAddressBinding.verifyAddress.setTypeface(roboto_condence);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            *//*default:
                return super.onOptionsItemSelected(item);*//*
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void sendVerifyAddress() {
        Log.d("TAG", "Submit lat: " + submittedLat);
        Log.d("TAG", "Submit lng: " + submittedLng);
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<VerifyAddressModel> call = apiInterface.verifyAddress(apiToken, taskList.getResponse().getConsignment().getConsignmentUniqueId(),
                String.valueOf(taskList.getResponse().getTasks().get(0).getTaskGroupId()),
                String.valueOf(submittedLat), String.valueOf(submittedLng));
        call.enqueue(new Callback<VerifyAddressModel>() {
            @Override
            public void onResponse(Call<VerifyAddressModel> call, Response<VerifyAddressModel> response) {
                verifyAddressModel = response.body();
                if (response.isSuccessful()) {
                    if (verifyAddressModel.getStatusCode() == 200) {
                        verifyAddressBinding.verifyAddress.setEnabled(true);
                        Toast.makeText(context, verifyAddressModel.getMessage().get(0).toString(), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, Summary.class);
                        intent.putExtra("stats", statisticsModel);
                        startActivity(intent);
                    } else {
                        verifyAddressBinding.verifyAddress.setEnabled(true);
                    }
                } else {
                    verifyAddressBinding.verifyAddress.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<VerifyAddressModel> call, Throwable t) {
                verifyAddressBinding.verifyAddress.setEnabled(true);
                Log.d("TAG", "response data: " + t.toString());
            }
        });

        /*Call<ResponseBody> call = apiInterface.verifyAddress(apiToken, taskList.getResponse().getConsignment().getConsignmentUniqueId(),
                String.valueOf(taskList.getResponse().getTasks().get(0).getTaskGroupId()),
                String.valueOf(submittedLat), String.valueOf(submittedLng));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.d("TAG", "response data: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG", "response data: " + t.toString());
            }
        });*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLongClickListener(this);
        if (checkPermission()) {
            isGpsEnable();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermission();
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mMap.clear();
        finalLatLong = latLng;
        MarkerOptions marker = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title("( " + latLng.latitude + " , " + latLng.longitude + " )");
        mMap.addMarker(marker);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flag == 1) {
            mapFragment.getMapAsync(this);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        }
    }

    private void GetMyLocation() {
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
    }

    GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {

        @Override
        public void onMyLocationChange(android.location.Location location) {
            if (gpsCounter == 0) {
                gpsCounter = 1;
                loc = new LatLng(location.getLatitude(), location.getLongitude());
                Log.d("TAG", "My current Position: " + loc);

                if (mMap != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15f));
                }
            }
        }
    };

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText(context, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGpsEnable();
                } else {
                    Toast.makeText(context, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void isGpsEnable() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        } else {
            showGPSDisabledAlertToUser();
        }
    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Biddyut Rider");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Location service disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go To Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                flag = 1;
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public GoogleMap.OnCameraChangeListener getCameraChangeListener() {
        return new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                if (currentZoom != position.zoom) {
                    {
                        currentZoom = position.zoom;
                        if (timecounter == 1) {
                            if (currentZoom < 12) {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(false);
                                }
                            } else {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(true);
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }
}
