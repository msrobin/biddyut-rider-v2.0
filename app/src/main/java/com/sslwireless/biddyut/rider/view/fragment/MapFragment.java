package com.sslwireless.biddyut.rider.view.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.MapFragmentBinding;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.model.utils.GoogleDirection;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private SupportMapFragment mapFragment = null;
    private Context context;
    private List<Marker> mMarkers = new ArrayList<Marker>();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private int flag = 0, gpsCounter = 0, timecounter = 0;
    private LatLng loc, oldLoc;
    private float currentZoom = -1;
    private TaskList taskList;
    private GoogleDirection gd;
    Document mDoc;
    LatLng result;
    private String title;
    private MapFragmentBinding mapFragmentBinding;
    private ApiInterface apiInterface;
    private View view = null;
    private String apiToken;
    private ProgressDialog dialogs;
    private boolean _hasLoadedOnce = false;

    public MapFragment() {
    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*bundle = getArguments();
        tasks = (TaskList) bundle.getSerializable("task_list");
        Log.d("TAG", "Task list : " + tasks);*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            mapFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false);
            view = mapFragmentBinding.getRoot();
            context = getActivity().getApplicationContext();
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            getApiToken(context);
        } else {
            _hasLoadedOnce = false;
            mapFragment.getMapAsync(this);
            getApiToken(context);
        }
        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkPermission()) {
            isGpsEnable();
        } else {
            requestPermission();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flag == 1) {
            mapFragment.getMapAsync(this);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        }
    }

    private void GetMyLocation() {
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
    }

    GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {

        @Override
        public void onMyLocationChange(android.location.Location location) {
            if (gpsCounter == 0) {
                gpsCounter = 1;
                loc = new LatLng(location.getLatitude(), location.getLongitude());
                Log.d("TAG", "My current Position: " + loc);
                oldLoc = loc;
                if (mMap != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 12f));
                }
            }
        }
    };

    private void getTaskList(String apiToken) {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskList> call = apiInterface.getTaskList(apiToken);
        call.enqueue(new Callback<TaskList>() {
            @Override
            public void onResponse(Call<TaskList> call, Response<TaskList> response) {
                taskList = response.body();
                if (response.isSuccessful()) {
                    Log.d("TAG", "status code: " + taskList.getStatusCode());
                    if (taskList.getStatusCode() == 200) {
                        //dialogs.dismiss();
                        setMarker();
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskList> call, Throwable t) {
                Log.d("TAG", "Failed message: " + t.toString());
            }
        });
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText(context, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGpsEnable();
                } else {
                    Toast.makeText(context, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
                break;
        }
    }

    private void isGpsEnable() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        } else {
            showGPSDisabledAlertToUser();
        }
    }

    private void setMarker() {
        for (int i = 0; i < taskList.getResponse().getTasks().size(); i++) {
            Task t = taskList.getResponse().getTasks().get(i);

            double list_lat = Double.parseDouble(t.getTaskLatitude());
            double list_lng = Double.parseDouble(t.getTaskLongitude());
            String status = t.getTaskStatus();
            title = t.getTaskTitle();

            Log.d("TAG", "list_lat " + list_lat);
            Log.d("TAG", "list_lng " + list_lng);

            result = new LatLng(list_lat, list_lng);
            Log.d("TAG", "latlng result: " + result);

            if (mMap != null) {
                mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = inflater.inflate(R.layout.custom_marker_info, null);

                        TextView task_title = (TextView) v.findViewById(R.id.task_title);
                        TextView task_status = (TextView) v.findViewById(R.id.task_status);

                        task_title.setText(marker.getTitle());
                        task_status.setText(marker.getSnippet());

                        return v;
                    }
                });
            }
            mMap.addMarker(new MarkerOptions().position(result).title(title).snippet(status).
                    icon(BitmapDescriptorFactory.fromResource(R.drawable.location)));
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Biddyut Rider");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Location service disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go To Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                flag = 1;
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public GoogleMap.OnCameraChangeListener getCameraChangeListener() {
        return new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                if (currentZoom != position.zoom) {
                    {
                        currentZoom = position.zoom;
                        if (timecounter == 1) {
                            if (currentZoom < 12) {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(false);
                                }
                            } else {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(true);
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                //_hasLoadedOnce = true;
                //progressD();
                Log.d("TAG", "Data get");
                getTaskList(apiToken);
            }
        }
    }

    public void progressD() {
        dialogs = new ProgressDialog(getActivity());
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }
}
