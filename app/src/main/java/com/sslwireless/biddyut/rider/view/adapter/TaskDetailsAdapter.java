package com.sslwireless.biddyut.rider.view.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.model.basic.SubmitModel;
import com.sslwireless.biddyut.rider.model.basic.SubmitTaskModel;
import com.sslwireless.biddyut.rider.model.response.details.Product;
import com.sslwireless.biddyut.rider.model.response.reason.StatusReason;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asif.malik on 7/25/2017.
 */

public class TaskDetailsAdapter extends RecyclerView.Adapter<TaskDetailsAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Product> mProductInfo;
    private ArrayList<SubmitTaskModel> mSubmitTaskModel = new ArrayList<SubmitTaskModel>(20);
    private Context mContext;
    private ClickListener clickListener;
    private int value, temp, holderPosition, adapterClearFlag;
    private Dialog dialog;
    private ApiInterface apiInterface;
    private StatusReason statusReason;
    private ArrayList<String> reasonOfFailure = new ArrayList<String>();
    private String apiToken;
    private ProgressDialog dialogs;
    private Spinner spinner;
    private String reason_checker, x;
    private Button dSubmitBtn;
    private EditText comment_additional_text;
    private SubmitModel mSubmitModel;
    private Typeface roboto_condence;
    private TextView reasonText, comment_additional;
    private TaskList mTaskList;
    private ArrayAdapter<String> adapter1 = null;
    private int finalResaonId, taskId;
    private Intent intent, intent2;

    public TaskDetailsAdapter(Context context, ArrayList<Product> data, ArrayList<SubmitTaskModel> submitTaskModel, SubmitModel submitModel, TaskList taskList) {
        inflater = LayoutInflater.from(context);
        this.mProductInfo = data;
        this.mContext = context;
        this.mSubmitTaskModel = submitTaskModel;
        this.mSubmitModel = submitModel;
        this.mTaskList = taskList;
        getApiToken(inflater.getContext());
        intent = new Intent("custom_message");
        intent2 = new Intent("custom_message_2");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_product_details_recycler, parent, false);
        MyViewHolder holder = new MyViewHolder(view, viewType);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Product productInfo = mProductInfo.get(position);
        mSubmitTaskModel.add(new SubmitTaskModel(productInfo.getTaskId(), productInfo.getQuantity(), "full", "", "", ""));

        Log.d("TAG", "title : " + productInfo.getTitle());
        Log.d("TAG", "quantity : " + productInfo.getQuantity());
        holder.name_of_product.setText(productInfo.getUniqueSuborderId());
        holder.product_quantity.setText(String.valueOf(productInfo.getQuantity()));

        mSubmitModel.setTemp_val(Integer.parseInt(holder.product_quantity.getText().toString()));

        if (holder.product_quantity.getText().toString().equals(String.valueOf(productInfo.getQuantity()))) {
            temp = 1;
        }

        holder.product_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == holder.product_minus.getId()) {
                    value = Integer.parseInt(holder.product_quantity.getText().toString());
                    temp = 0;
                    if (holder.product_quantity.getText().toString().equals("0")) {
                        Toast.makeText(inflater.getContext(), "You can not decrease product value", Toast.LENGTH_SHORT).show();
                    } else {
                        holder.product_quantity.setText(String.valueOf(--value));
                        mSubmitTaskModel.get(position).setQuantity(Integer.parseInt(holder.product_quantity.getText().toString()));

                        if (holder.product_quantity.getText().toString().equals("0")) {
                            mSubmitTaskModel.get(position).setStatus("fail");
                        } else if (productInfo.getQuantity().equals(holder.product_quantity.getText().toString())) {
                            mSubmitTaskModel.get(position).setStatus("full");
                        } else {
                            mSubmitTaskModel.get(position).setStatus("partial");
                        }
                        intent.putExtra("quantity", holder.product_quantity.getText().toString());
                        LocalBroadcastManager.getInstance(inflater.getContext()).sendBroadcast(intent);

                        intent2.putExtra("alert", 1);
                        LocalBroadcastManager.getInstance(inflater.getContext()).sendBroadcast(intent2);
                    }
                }
            }
        });

        holder.product_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == holder.product_plus.getId()) {
                    value = Integer.parseInt(holder.product_quantity.getText().toString());
                    if (holder.product_quantity.getText().toString().equals(String.valueOf(productInfo.getQuantity()))) {
                        Toast.makeText(inflater.getContext(), "You can not increase product value", Toast.LENGTH_SHORT).show();
                        temp = 1;
                    } else {
                        mSubmitTaskModel.get(position).setQuantity(Integer.parseInt(holder.product_quantity.getText().toString()));
                        holder.product_quantity.setText(String.valueOf(++value));

                        //productInfo.getQuantity().equals(holder.product_quantity.getText().toString())
                        if (holder.product_quantity.getText().toString().equals("0")) {
                            mSubmitTaskModel.get(position).setStatus("fail");
                        } else if (holder.product_quantity.getText().toString().equals(String.valueOf(productInfo.getQuantity()))) {
                            mSubmitTaskModel.get(position).setStatus("full");
                        } else {
                            mSubmitTaskModel.get(position).setStatus("partial");
                        }
                        intent.putExtra("quantity", holder.product_quantity.getText().toString());
                        LocalBroadcastManager.getInstance(inflater.getContext()).sendBroadcast(intent);

                        if (holder.product_quantity.getText().toString().equals(String.valueOf(productInfo.getQuantity()))) {
                            intent2.putExtra("alert", 0);
                        } else {
                            intent2.putExtra("alert", 1);
                        }
                        LocalBroadcastManager.getInstance(inflater.getContext()).sendBroadcast(intent2);
                    }
                }
            }
        });

        holder.product_reason_remarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressD();
                holderPosition = holder.getAdapterPosition();
                Product id = mProductInfo.get(holderPosition);
                taskId = id.getTaskId();
                Log.d("TAG", "Task Id is: " + taskId);
                dialog = new Dialog(inflater.getContext(), R.style.deals4uCustomDialog);
                dialog.setContentView(R.layout.custom_remarks_comment);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                spinner = (Spinner) dialog.findViewById(R.id.comment_failure_spinner);
                dSubmitBtn = (Button) dialog.findViewById(R.id.submit_btn);
                comment_additional_text = (EditText) dialog.findViewById(R.id.comment_additional_text);
                reasonText = (TextView) dialog.findViewById(R.id.reasonText);
                comment_additional = (TextView) dialog.findViewById(R.id.comment_additional);

                dSubmitBtn.setTypeface(roboto_condence);
                comment_additional_text.setTypeface(roboto_condence);
                reasonText.setTypeface(roboto_condence);
                comment_additional.setTypeface(roboto_condence);

                if (holder.product_quantity.getText().toString().equals(String.valueOf(productInfo.getQuantity()))) {
                    reasonText.setVisibility(View.GONE);
                    spinner.setVisibility(View.GONE);
                } else {
                    reasonText.setVisibility(View.VISIBLE);
                    spinner.setVisibility(View.VISIBLE);
                }

                DisplayMetrics metrics = inflater.getContext().getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                int height = metrics.heightPixels;
                dialog.getWindow().setLayout(width, height - 640);
                dialog.getWindow().setGravity(Gravity.BOTTOM);
                dialog.show();

                apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                Call<StatusReason> call = apiInterface.statusReason(apiToken, mTaskList.getResponse().getConsignment().getConsignmentType());
                call.enqueue(new Callback<StatusReason>() {
                    @Override
                    public void onResponse(Call<StatusReason> call, Response<StatusReason> response) {
                        statusReason = response.body();
                        if (statusReason.getStatusCode() == 200 && statusReason.getStatus().equals("success")) {
                            if (adapterClearFlag == 0) {
                                adapterClearFlag = 1;
                                setSpinnerData();
                            } else {
                                adapter1.clear();
                                setSpinnerData();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusReason> call, Throwable t) {

                    }
                });

                dSubmitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (reasonText.getVisibility() == View.VISIBLE && spinner.getVisibility() == View.VISIBLE) {
                            if (reason_checker != null) {
                                for (int j = 0; j < statusReason.getResponse().size(); j++) {
                                    x = statusReason.getResponse().get(j).getReason();

                                    if (x.equals(reason_checker)) {
                                        intent2.putExtra("alert", 0);
                                        LocalBroadcastManager.getInstance(inflater.getContext()).sendBroadcast(intent2);
                                        dialog.dismiss();
                                        finalResaonId = statusReason.getResponse().get(j).getId();
                                        mSubmitTaskModel.get(position).setReason_id(String.valueOf(finalResaonId));
                                    }
                                }

                                if (comment_additional_text.getText().toString().isEmpty()) {
                                    dialog.dismiss();
                                    mSubmitTaskModel.get(position).setRemarks("");
                                } else {
                                    dialog.dismiss();
                                    mSubmitTaskModel.get(position).setRemarks(comment_additional_text.getText().toString());
                                }
                            } else {
                                Toast.makeText(inflater.getContext(), "Please select reason", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            if (comment_additional_text.getText().toString().isEmpty()) {
                                dialog.dismiss();
                                mSubmitTaskModel.get(position).setRemarks("");
                            } else {
                                dialog.dismiss();
                                mSubmitTaskModel.get(position).setRemarks(comment_additional_text.getText().toString());
                            }
                        }
                    }
                });
            }
        });
    }

    private void setSpinnerData() {
        dialogs.dismiss();
        reasonOfFailure.add("");

        for (int i = 0; i < statusReason.getResponse().size(); i++) {
            reasonOfFailure.add(statusReason.getResponse().get(i).getReason());
        }

        adapter1 = new ArrayAdapter<String>(
                inflater.getContext(),
                R.layout.custom_spinner,
                reasonOfFailure
        ) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                Typeface externalFont = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                ((TextView) v).setTypeface(externalFont);
                return v;
            }
        };
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reason_checker = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return mProductInfo.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name_of_product, product_quantity;
        ImageView product_minus, product_plus, product_reason_remarks;

        public MyViewHolder(View itemView, int viewType) {
            super(itemView);
            itemView.setOnClickListener(this);
            roboto_condence = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/RobotoCondensed-Bold.ttf");
            name_of_product = (TextView) itemView.findViewById(R.id.name_of_product);
            product_quantity = (TextView) itemView.findViewById(R.id.product_quantity);
            product_minus = (ImageView) itemView.findViewById(R.id.product_minus);
            product_plus = (ImageView) itemView.findViewById(R.id.product_plus);
            product_reason_remarks = (ImageView) itemView.findViewById(R.id.product_reason_remarks);

            name_of_product.setTypeface(roboto_condence);
            product_quantity.setTypeface(roboto_condence);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.itemClicked(v, getPosition());
            }
        }
    }

    public interface ClickListener {
        public void itemClicked(View v, int position);

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    public void progressD() {
        dialogs = new ProgressDialog(inflater.getContext());
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }
}
