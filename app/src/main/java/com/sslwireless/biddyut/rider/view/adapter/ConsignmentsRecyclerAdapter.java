package com.sslwireless.biddyut.rider.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.model.response.consignments.Consignment;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.activity.Consignments;
import com.sslwireless.biddyut.rider.view.activity.RouteActivity;

import java.util.ArrayList;

/**
 * Created by asif.malik on 9/10/2017.
 */

public class ConsignmentsRecyclerAdapter extends RecyclerView.Adapter<ConsignmentsRecyclerAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Consignment> mConsignment;
    private Context mContext;
    private ClickListener clickListener;
    private Typeface roboto_condence;
    private int viewVisibilty = 0;

    public ConsignmentsRecyclerAdapter(Context context, ArrayList<Consignment> data) {
        inflater = LayoutInflater.from(context);
        this.mConsignment = data;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_consignments_recycler, parent, false);
        MyViewHolder holder = new MyViewHolder(view, viewType);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Consignment consignment = mConsignment.get(position);

        holder.con_id_val.setText(consignment.getConsignmentUniqueId());
        holder.con_type_val.setText(consignment.getType());
        holder.con_status_val.setText(consignment.getStatus());

        holder.con_req_val.setText(String.valueOf(consignment.getCollectedAmount() + " BDT"));
        holder.con_collected_val.setText(String.valueOf(consignment.getCollectedAmount() + " BDT"));
        holder.con_quantity_val.setText(String.valueOf(consignment.getRequestedQuantity()));
        holder.con_success_val.setText(String.valueOf(consignment.getSuccessQuantity()));
        holder.con_return_val.setText(String.valueOf(consignment.getReturnQuantity()));
        holder.con_distance_val.setText(String.valueOf(consignment.getDistance()));

        if (consignment.getStatus().equals("Started")) {
            holder.btn_recon.setVisibility(View.VISIBLE);
        }

        holder.first_lay_part.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewVisibilty == 0) {
                    viewVisibilty = 1;
                    holder.second_lay_part.setVisibility(View.VISIBLE);
                } else {
                    viewVisibilty = 0;
                    holder.second_lay_part.setVisibility(View.GONE);
                }
            }
        });

        holder.btn_recon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.itemClicked(v, position, "recon");
            }
        });

        holder.btn_route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.itemClicked(v, position, "map");
            }
        });
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return mConsignment.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {

        TextView con_id, con_id_val, con_type, con_type_val, con_status, con_status_val, con_req, con_req_val,
                con_collected, con_collected_val, con_quantity, con_quantity_val, con_success, con_success_val,
                con_return, con_return_val, con_distance, con_distance_val;
        LinearLayout first_lay_part, second_lay_part;
        Button btn_recon, btn_route;

        public MyViewHolder(View itemView, int viewType) {
            super(itemView);
            //itemView.setOnClickListener(this);
            roboto_condence = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/RobotoCondensed-Bold.ttf");

            con_id = (TextView) itemView.findViewById(R.id.con_id);
            con_id_val = (TextView) itemView.findViewById(R.id.con_id_val);
            con_type = (TextView) itemView.findViewById(R.id.con_type);
            con_type_val = (TextView) itemView.findViewById(R.id.con_type_val);
            con_status = (TextView) itemView.findViewById(R.id.con_status);
            con_status_val = (TextView) itemView.findViewById(R.id.con_status_val);
            con_req = (TextView) itemView.findViewById(R.id.con_req);
            con_req_val = (TextView) itemView.findViewById(R.id.con_req_val);
            con_collected = (TextView) itemView.findViewById(R.id.con_collected);
            con_collected_val = (TextView) itemView.findViewById(R.id.con_collected_val);
            con_quantity = (TextView) itemView.findViewById(R.id.con_quantity);
            con_quantity_val = (TextView) itemView.findViewById(R.id.con_quantity_val);
            con_success = (TextView) itemView.findViewById(R.id.con_success);
            con_success_val = (TextView) itemView.findViewById(R.id.con_success_val);
            con_return = (TextView) itemView.findViewById(R.id.con_return);
            con_return_val = (TextView) itemView.findViewById(R.id.con_return_val);
            con_distance = (TextView) itemView.findViewById(R.id.con_distance);
            con_distance_val = (TextView) itemView.findViewById(R.id.con_distance_val);

            first_lay_part = (LinearLayout) itemView.findViewById(R.id.first_lay_part);
            second_lay_part = (LinearLayout) itemView.findViewById(R.id.second_lay_part);

            btn_recon = (Button) itemView.findViewById(R.id.btn_recon);
            btn_route = (Button) itemView.findViewById(R.id.btn_route);

            con_id.setTypeface(roboto_condence);
            con_id_val.setTypeface(roboto_condence);
            con_type.setTypeface(roboto_condence);
            con_type_val.setTypeface(roboto_condence);
            con_status.setTypeface(roboto_condence);
            con_status_val.setTypeface(roboto_condence);
            con_req.setTypeface(roboto_condence);
            con_req_val.setTypeface(roboto_condence);
            con_collected.setTypeface(roboto_condence);
            con_collected_val.setTypeface(roboto_condence);
            con_quantity.setTypeface(roboto_condence);
            con_quantity_val.setTypeface(roboto_condence);
            con_success.setTypeface(roboto_condence);
            con_success_val.setTypeface(roboto_condence);
            con_return.setTypeface(roboto_condence);
            con_return_val.setTypeface(roboto_condence);
            con_distance.setTypeface(roboto_condence);
            con_distance_val.setTypeface(roboto_condence);

            btn_recon.setTypeface(roboto_condence);
            btn_route.setTypeface(roboto_condence);
        }

        /*@Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.itemClicked(v, getPosition());
            }
        }*/
    }

    public interface ClickListener {
        public void itemClicked(View v, int position, String tag);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

