package com.sslwireless.biddyut.rider.view.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.StartedTaskBinding;
import com.sslwireless.biddyut.rider.model.basic.SubmitModel;
import com.sslwireless.biddyut.rider.model.basic.SubmitTaskModel;
import com.sslwireless.biddyut.rider.model.response.stask.StartTask;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.model.utils.GoogleDirection;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import org.w3c.dom.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asif.malik on 7/31/2017.
 */

public class StartedTaskActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private Context context;
    private List<Marker> mMarkers = new ArrayList<Marker>();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private int flag = 0, gpsCounter = 0, timecounter = 0;
    private LatLng loc;
    private float currentZoom = -1;
    private TaskList tasks;
    private GoogleDirection gd;
    Document mDoc;
    LatLng result;
    private String title, task_title, apiToken;
    private StartedTaskBinding startedTaskBinding;
    private int task_group_id;
    private ApiInterface apiInterface;
    private StartTask startTask;
    private String taskStatus;
    private SubmitModel submitModel;
    private Typeface roboto_condence;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startedTaskBinding = DataBindingUtil.setContentView(this, R.layout.activity_started_task);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.start_task_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");

        submitModel = new SubmitModel();

        getApiToken(context);
        getSaveTask(context);

        if (getIntent().hasExtra("task_list")) {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.started_map);
            mapFragment.getMapAsync(this);
            tasks = (TaskList) getIntent().getSerializableExtra("task_list");
            task_title = getIntent().getStringExtra("task_title");
            task_group_id = getIntent().getIntExtra("task_group_id", 0);
        }

        startedTaskBinding.showDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartedTaskActivity.this, TaskDetailsActivity.class);
                intent.putExtra("token", apiToken);
                intent.putExtra("consignment_unique_id", tasks.getResponse().getConsignment().getConsignmentUniqueId());
                intent.putExtra("task_group_id", task_group_id);
                intent.putExtra("consignment_type", tasks.getResponse().getConsignment().getConsignmentType());
                startActivity(intent);
            }
        });

        startedTaskBinding.completeTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartedTaskActivity.this, Comment.class);
                intent.putExtra("token", apiToken);
                intent.putExtra("consignment_unique_id", tasks.getResponse().getConsignment().getConsignmentUniqueId());
                intent.putExtra("task_group_id", task_group_id);
                intent.putExtra("task_list", tasks);
                intent.putExtra("submit_task", submitModel);
                startActivity(intent);
            }
        });
        startedTaskBinding.completeTask.setTypeface(roboto_condence);
        startedTaskBinding.showDetails.setTypeface(roboto_condence);
    }

    private void startTask(String apiToken, String con_id, String task_id, String lat, String lon) {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StartTask> call = apiInterface.startTask(apiToken, con_id, task_id, lat, lon);
        call.enqueue(new Callback<StartTask>() {
            @Override
            public void onResponse(Call<StartTask> call, Response<StartTask> response) {
                startTask = response.body();
                //saveTask("1", context);
                //Log.d("TAG", "Message is: " + startTask.getMessage().get(0).toString());
            }

            @Override
            public void onFailure(Call<StartTask> call, Throwable t) {

            }
        });

        /*apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.startTask(apiToken, con_id, task_id, lat, lon);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.e("asif", "onResponse: " + response.body().string());
                    Log.e("asif", "onResponse: " + response.code());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("asif", "onFailure: " + t.toString());
            }
        });*/
    }

    public String getSaveTask(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        taskStatus = preferences.getString("task_status", "null");
        Log.d("TAG", "Message is shared Preference: " + taskStatus);
        return taskStatus;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkPermission()) {
            isGpsEnable();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermission();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flag == 1) {
            mapFragment.getMapAsync(this);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        }
    }

    private void GetMyLocation() {
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
    }

    GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {

        @Override
        public void onMyLocationChange(android.location.Location location) {
            if (gpsCounter == 0) {
                gpsCounter = 1;
                loc = new LatLng(location.getLatitude(), location.getLongitude());
                Log.d("TAG", "My current Position: " + loc);

                double start_lat = location.getLatitude();
                double start_lon = location.getLongitude();

                String consignment_unique_id = tasks.getResponse().getConsignment().getConsignmentUniqueId();
                setMarker(consignment_unique_id, start_lat, start_lon);

                if (mMap != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15f));
                }
            }
        }
    };

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText(context, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGpsEnable();
                } else {
                    Toast.makeText(context, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void isGpsEnable() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        } else {
            showGPSDisabledAlertToUser();
        }
    }

    private void isConnectingToInternet(LatLng loc, LatLng res) {
        final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.isConnected()) {
            CalculateRoute(loc, res);
        } else {
            showNetworkAlertDialog();
        }
    }

    private void showNetworkAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(StartedTaskActivity.this);
        alertDialogBuilder.setMessage("Please Enable Your Network Connection First...").setTitle("Network Connecting Issue")
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void setMarker(String con, double lat, double lng) {
        for (int i = 0; i < tasks.getResponse().getTasks().size(); i++) {
            Task t = tasks.getResponse().getTasks().get(i);
            title = t.getTaskTitle();

            if (title.equals(task_title)) {
                double list_lat = Double.parseDouble(t.getTaskLatitude());
                double list_lng = Double.parseDouble(t.getTaskLongitude());
                result = new LatLng(list_lat, list_lng);
                submitModel.setEnd_lat(list_lat);
                submitModel.setEnd_lon(list_lng);

                Log.d("TAG", "Location result: " + result);

                if (list_lat == 0.0 || list_lng == 0.0) {
                    Toast.makeText(context, "Destination route not found", Toast.LENGTH_SHORT).show();
                } else {
                    isConnectingToInternet(loc, result);
                }
            }
        }

        startTask(apiToken, con, String.valueOf(task_group_id),
                String.valueOf(lat), String.valueOf(lng));
    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Biddyut Rider");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Location service disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go To Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                flag = 1;
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public GoogleMap.OnCameraChangeListener getCameraChangeListener() {
        return new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                if (currentZoom != position.zoom) {
                    {
                        currentZoom = position.zoom;
                        if (timecounter == 1) {
                            if (currentZoom < 12) {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(false);
                                }
                            } else {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(true);
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public void CalculateRoute(final LatLng loc, final LatLng result) {
        gd = new GoogleDirection(context);

        gd.setOnDirectionResponseListener(new GoogleDirection.OnDirectionResponseListener() {
            public void onResponse(String status, Document doc, GoogleDirection gd) {

                mDoc = doc;

                String googleDrectionResult = gd.getStatus(mDoc);
                Log.d("TAG", "googleDrectionResult: " + googleDrectionResult);

                if (googleDrectionResult.equals("OK")) {
                    mMap.addMarker(new MarkerOptions().title(task_title).snippet("Estimated Travel Time: " + gd.getTotalDurationText(mDoc) + " " + "(" + gd.getTotalDistanceText(mDoc) + ")").position(result)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_alt)));

                    ArrayList<LatLng> directionPoint = gd.getDirection(doc);
                    PolylineOptions rectLine = new PolylineOptions().width(10).color(Color.parseColor("#E61E27"));

                    for (int i = 0; i < directionPoint.size(); i++) {
                        rectLine.add(directionPoint.get(i));
                    }
                    Polyline polylin = mMap.addPolyline(rectLine);

                    String[] temp_splitter = gd.getTotalDistanceText(mDoc).split(" ");
                    String splitDistance = temp_splitter[0];
                    //Log.d("TAG","Submit distance model: " + splitDistance);
                    submitModel.setDistance(splitDistance);

                } else {
                    Toast.makeText(context, "Destination route not found", Toast.LENGTH_LONG).show();
                }
            }
        });
        gd.request(loc, result, GoogleDirection.MODE_DRIVING);
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            /*default:
                return super.onOptionsItemSelected(item);*/
        }
        return super.onOptionsItemSelected(item);
    }
}
