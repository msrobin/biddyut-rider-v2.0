package com.sslwireless.biddyut.rider.view.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.graphics.BitmapCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.VerificationDoneBinding;
import com.sslwireless.biddyut.rider.model.basic.SubmitModel;
import com.sslwireless.biddyut.rider.model.basic.SubmitTaskModel;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;
import com.sslwireless.biddyut.rider.model.response.stats.StatisticsModel;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Asif Malik on 7/20/2017.
 */

public class VerificationDone extends AppCompatActivity {

    private VerificationDoneBinding verificationDoneBinding;
    private SubmitTaskModel submitTaskModel;
    private Context context;
    private ApiInterface apiInterface;
    private StatisticsModel statisticsModel;
    private String consignment_id, token;
    private int task_group_id;
    private Bitmap picture, signature;
    private MultipartBody.Part pictureBody, signatureBody;
    private String imagePath;
    private SubmitModel submitModel;
    private ProgressDialog dialogs;
    private TaskList taskList;
    private TaskDetails taskDetails;
    private int flag = 0, taskCompleteFlag = 0;
    private ArrayList<SubmitTaskModel> submitTaskModelList = new ArrayList<SubmitTaskModel>();
    private Toolbar toolbar;
    private Typeface roboto_condence;
    private File f;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verificationDoneBinding = DataBindingUtil.setContentView(this, R.layout.verification_done);

        context = this;
        progressD();

        toolbar = (Toolbar) findViewById(R.id.veri_done_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        initTypeface();

        token = getIntent().getStringExtra("token");
        consignment_id = getIntent().getStringExtra("consignment_unique_id");
        task_group_id = getIntent().getIntExtra("task_group_id", 0);
        submitTaskModel = (SubmitTaskModel) getIntent().getSerializableExtra("submit_task_model");
        submitModel = (SubmitModel) getIntent().getSerializableExtra("submit_task");
        submitTaskModelList = (ArrayList<SubmitTaskModel>) getIntent().getSerializableExtra("submit_task_model_list");
        Log.d("TAG", "submitTaskModelList done: " + submitTaskModelList.size());

        if (getIntent().getByteArrayExtra("signature_img") != null) {
            signature = BitmapFactory.decodeByteArray(getIntent().getByteArrayExtra("signature_img"), 0, getIntent()
                    .getByteArrayExtra("signature_img").length);
            Log.d("TAG", "Signature: " + signature.getByteCount());

        }
        taskList = (TaskList) getIntent().getSerializableExtra("task_list");

        imagePath = submitModel.getPhoto();
        Log.d("TAG", "image value: " + imagePath);
        setupImage(imagePath);

        verificationDoneBinding.linearLayout5.setVisibility(View.GONE);
        verificationDoneBinding.veriDoneTaskText.setVisibility(View.GONE);

        getTaskDetails();

        verificationDoneBinding.verificationCaptureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressD();
                sendSubmitTask(token, consignment_id, task_group_id, String.valueOf(submitModel.getEnd_lat()), String.valueOf(submitModel.getEnd_lon())
                        , submitModel.getDistance(), submitTaskModel);
            }
        });

        verificationDoneBinding.verificationDoneBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VerificationDone.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        verificationDoneBinding.verificationFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag == 1) {
                    Intent intent = new Intent(VerificationDone.this, Summary.class);
                    intent.putExtra("stats", statisticsModel);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "Please submit task first", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initTypeface() {
        verificationDoneBinding.verificationTitle.setTypeface(roboto_condence);
        //verificationDoneBinding.veriDoneEmail.setTypeface(roboto_condence);
        verificationDoneBinding.veriDoneEmaill.setTypeface(roboto_condence);
        verificationDoneBinding.veriDoneName.setTypeface(roboto_condence);
        verificationDoneBinding.verificationTitle.setTypeface(roboto_condence);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupImage(String imagePath) {
        try {
            picture = BitmapFactory.decodeFile(imagePath);
            Log.d("TAG", "Image size before scaling: " + picture.getByteCount() + "Width: " + picture.getWidth() + "Height: " + picture.getHeight());
            picture = Bitmap.createScaledBitmap(picture, picture.getWidth() / 15, picture.getHeight() / 15, true);
            Log.d("TAG", "Image size before scaling: " + picture.getByteCount() + "Width: " + picture.getWidth() + "Height: " + picture.getHeight());
        } catch (Exception ignored) {

        }
    }

    private void sendSubmitTask(String token, String consignment_id, int task_group_id, String submit_lat, String submit_lng,
                                String submit_dis, SubmitTaskModel submitTaskModel) {
        try {
            ByteArrayOutputStream imageStream = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.PNG, 100, imageStream);
            RequestBody requestFileForImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageStream.toByteArray());
            pictureBody = MultipartBody.Part.createFormData("photo", "photo_" + System.currentTimeMillis() + ".jpg", requestFileForImage);

            ByteArrayOutputStream coolerPictureStream = new ByteArrayOutputStream();
            signature.compress(Bitmap.CompressFormat.PNG, 100, coolerPictureStream);
            RequestBody requestFileForSignature = RequestBody.create(MediaType.parse("multipart/form-data"), coolerPictureStream.toByteArray());
            signatureBody = MultipartBody.Part.createFormData("signature", "signature_" + System.currentTimeMillis() + ".jpg", requestFileForSignature);

        } catch (Exception ignored) {
            Log.d("TAG", "ignored image:" + ignored.getMessage());
        }

        RequestBody dis;
        if (submit_dis != null) {
            dis = RequestBody.create(MediaType.parse("multipart/form-data"), submit_dis);
        } else {
            dis = RequestBody.create(MediaType.parse("multipart/form-data"), "0.0");
        }

        RequestBody token_val = RequestBody.create(MediaType.parse("multipart/form-data"), token);
        RequestBody consignment_val = RequestBody.create(MediaType.parse("multipart/form-data"), consignment_id);
        RequestBody task_group_val = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(task_group_id));
        RequestBody lat = RequestBody.create(MediaType.parse("multipart/form-data"), submit_lat);
        RequestBody lng = RequestBody.create(MediaType.parse("multipart/form-data"), submit_lng);
        RequestBody products = RequestBody.create(MediaType.parse("multipart/form-data"), new Gson().toJson(submitTaskModelList));

        Log.d("TAG", "Total pro: " + new Gson().toJson(submitTaskModelList));
        Log.d("TAG", "Array list size: " + submitTaskModelList.size());
        Log.d("TAG", "Total token: " + token);
        Log.d("TAG", "Total consign: " + consignment_id);
        Log.d("TAG", "Total task group: " + String.valueOf(task_group_id));
        Log.d("TAG", "Total lat: " + submit_lat);
        Log.d("TAG", "Total lng: " + submit_lng);
        Log.d("TAG", "Total dis: " + submit_dis);
        //dialogs.dismiss();
        //Log.d("TAG", "Total gson size: " + submitTaskModels.size());

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatisticsModel> call = apiInterface.sendSubmitTask(token_val, consignment_val,
                task_group_val, lat, lng, dis, products, pictureBody, signatureBody);
        call.enqueue(new Callback<StatisticsModel>() {
            @Override
            public void onResponse(Call<StatisticsModel> call, Response<StatisticsModel> response) {
                dialogs.dismiss();
                statisticsModel = response.body();
                if (response.isSuccessful()) {
                    if (statisticsModel.getStatusCode() == 200) {
                        flag = 1;
                        taskCompleteFlag = 1;
                        Toast.makeText(context, statisticsModel.getMessage().get(0), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(VerificationDone.this, VerifyAddressActivity.class);
                        intent.putExtra("task_list", taskList);
                        intent.putExtra("stats", statisticsModel);
                        startActivity(intent);
                    } else if (statisticsModel.getStatusCode() == 401) {
                        Toast.makeText(context, "Error while uploading data!!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, statisticsModel.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, statisticsModel.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StatisticsModel> call, Throwable t) {
                dialogs.dismiss();
                Toast.makeText(context, "Something went wrong. Please check again.", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "Failed data: " + t.toString());
            }
        });

        /*apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.sendSubmitTask(token_val, consignment_val, task_group_val,
                lat, lng, dis, products, pictureBody, signatureBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialogs.dismiss();
                //Log.d("TAG", "response data from server: " + response.body().string());
                Log.d("TAG", "response data from server: " + response.code());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TAG", "Failed data: " + t.toString());
            }
        });*/
    }

    private void saveTask(String taskStatus, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("task_status", taskStatus);
        editor.commit();
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }

    private void getTaskDetails() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskDetails> call = apiInterface.getTaskDetails(token, consignment_id, String.valueOf(task_group_id));
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                dialogs.dismiss();
                taskDetails = response.body();
                verificationDoneBinding.setVerificationDoneData(taskDetails);
                Glide.with(context).load(R.drawable.avatar).into(verificationDoneBinding.imageView);
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (taskCompleteFlag == 1) {
            showAlertToUser();
        } else {
            super.onBackPressed();
        }
    }

    private void showAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Biddyut Rider");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Do you want go back?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}
