package com.sslwireless.biddyut.rider.view.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.location.LocationListener;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.sslwireless.biddyut.rider.model.basic.BasicModel;
import com.sslwireless.biddyut.rider.model.basic.BasicResponse;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asif.malik on 9/6/2017.
 */

public class RiderLocationService extends Service {

    PowerManager.WakeLock wakeLock;
    private LocationManager locationManager;
    LatLng loc;
    private String apiToken, consign_id;
    private ApiInterface apiInterface;
    private BasicResponse basicResponse;
    private int f;

    public RiderLocationService() {
        // TODO Auto-generated constructor stub
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        getApiToken(getApplicationContext());
        getConsignmentId(getApplicationContext());

        PowerManager pm = (PowerManager) getSystemService(this.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");
        Log.e("Google", "Service Created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Google", "Service Started");
        locationManager = (LocationManager) getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return START_STICKY;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                60 * 1000 * 10, 25, listener);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                60 * 1000 * 10, 25, listener);

        return START_STICKY;
    }

    private LocationListener listener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub
            //Toast.makeText(getApplicationContext(), "Location Changed", Toast.LENGTH_LONG).show();
            Log.e("Google", "Location Changed");
            if (location == null)
                return;

            if (isConnectingToInternet(getApplicationContext())) {
                loc = new LatLng(location.getLatitude(), location.getLongitude());
                Log.d("gg", "My Location: " + loc);
                passRiderLocation(apiToken, location.getLatitude(), location.getLongitude());
            }

        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }
    };

    private void passRiderLocation(String apiToken, double lat, double lng) {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<BasicResponse> call = apiInterface.sendRiderData(apiToken, consign_id, String.valueOf(lat), String.valueOf(lng));
        call.enqueue(new Callback<BasicResponse>() {
            @Override
            public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                basicResponse = response.body();
                if (response.isSuccessful()) {
                    if (basicResponse.getStatusCode() == 200) {
                        //Toast.makeText(getApplicationContext(), basicResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicResponse> call, Throwable t) {
                Log.d("TAG", "response data: " + t.toString());
            }
        });
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        wakeLock.release();
        super.onDestroy();
        //
    }

    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    public String getConsignmentId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        consign_id = preferences.getString("consign_id", "null");
        Log.d("TAG", "consign_id: " + consign_id);
        return consign_id;
    }
}

