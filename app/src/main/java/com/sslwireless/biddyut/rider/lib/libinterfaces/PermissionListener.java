package com.sslwireless.biddyut.rider.lib.libinterfaces;

/**
 * Created by SSL_ZAHID on 8/30/2016.
 */
public interface PermissionListener {
    void permissionGranted();
    void permissionDenied(int position);
}
