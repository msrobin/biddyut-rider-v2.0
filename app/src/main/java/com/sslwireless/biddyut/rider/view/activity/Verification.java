package com.sslwireless.biddyut.rider.view.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.VerificationBinding;
import com.sslwireless.biddyut.rider.model.basic.SubmitModel;
import com.sslwireless.biddyut.rider.model.basic.SubmitTaskModel;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Asif Malik on 7/20/2017.
 */

public class Verification extends AppCompatActivity {

    private VerificationBinding verificationBinding;
    private Context context;
    static final int REQUEST_IMAGE_CAPTURE = 1, REQUEST_GALLERY = 2;
    private Bitmap mImageBitmap;
    private String mCurrentPhotoPath;
    private File image;
    private Bitmap signatureImage;
    private TaskDetails taskDetails;
    private ApiInterface apiInterface;
    private String consignment_id, token, consignment_type, imageFileName;
    private int task_group_id;
    private ProgressDialog dialogs;
    private SubmitTaskModel submitTaskModel;
    private SubmitModel submitModel;
    private ByteArrayOutputStream bs;
    private TaskList taskList;
    private int MY_READ_PERMISSION_REQUEST_CODE = 1;
    private int PICK_IMAGE_REQUEST;
    private ArrayList<SubmitTaskModel> submitTaskModelList = new ArrayList<SubmitTaskModel>();
    private Toolbar toolbar;
    private Typeface roboto_condence;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verificationBinding = DataBindingUtil.setContentView(this, R.layout.verification);
        context = this;
        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        toolbar = (Toolbar) findViewById(R.id.verification_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        token = getIntent().getStringExtra("token");
        consignment_id = getIntent().getStringExtra("consignment_unique_id");
        consignment_type = getIntent().getStringExtra("consignment_type");
        task_group_id = getIntent().getIntExtra("task_group_id", 0);
        submitModel = (SubmitModel) getIntent().getSerializableExtra("submit_task");
        submitTaskModel = (SubmitTaskModel) getIntent().getSerializableExtra("submit_task_model");
        taskList = (TaskList) getIntent().getSerializableExtra("task_list");
        submitTaskModelList = (ArrayList<SubmitTaskModel>) getIntent().getSerializableExtra("submit_task_model_list");
        Log.d("TAG", "submitTaskModelList ver: " + submitTaskModelList.size());

        verificationBinding.verificationCaptureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PICK_IMAGE_REQUEST = 1;
                checkGalleryPermission();
            }
        });

        verificationBinding.verificationFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PICK_IMAGE_REQUEST = 2;
                checkGalleryPermission();
            }
        });

        verificationBinding.verificationSignatureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSignature();
            }
        });

        verificationBinding.verificationProceedLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Verification.this, VerificationDone.class);
                intent.putExtra("token", token);
                intent.putExtra("consignment_unique_id", consignment_id);
                intent.putExtra("task_group_id", task_group_id);
                intent.putExtra("submit_task", submitModel);
                intent.putExtra("submit_task_model", submitTaskModel);
                intent.putExtra("task_list", taskList);
                intent.putExtra("submit_task_model_list", submitTaskModelList);
                if (signatureImage == null) {
                    intent.putExtra("signature_img", "");
                } else {
                    intent.putExtra("signature_img", bs.toByteArray());
                }
                startActivity(intent);
                //}
            }
        });
        progressD();
        getTaskDetails();
        initTypeface();
    }

    private void initTypeface() {
        verificationBinding.verificationTitle.setTypeface(roboto_condence);
        verificationBinding.verificationProceed.setTypeface(roboto_condence);
        verificationBinding.textView3.setTypeface(roboto_condence);
        verificationBinding.textView4.setTypeface(roboto_condence);
        //verificationBinding.textView5.setTypeface(roboto_condence);
        verificationBinding.sign.setTypeface(roboto_condence);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            /*default:
                return super.onOptionsItemSelected(item);*/
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkGalleryPermission() {
        if (ContextCompat.checkSelfPermission(Verification.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            if ((ContextCompat.checkSelfPermission(Verification.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED))
                if (PICK_IMAGE_REQUEST == 2) {
                    fromCamera();
                } else {
                    captureImageUsingCamera();
                }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        MY_READ_PERMISSION_REQUEST_CODE);
            } else {
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[]
            permissions, int[] grantResults) {
        if (requestCode == MY_READ_PERMISSION_REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            captureImageUsingCamera();
        }
    }

    private void getTaskDetails() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskDetails> call = apiInterface.getTaskDetails(token, consignment_id, String.valueOf(task_group_id));
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                dialogs.dismiss();
                taskDetails = response.body();
                verificationBinding.setVerificationData(taskDetails);
                Glide.with(context).load(R.drawable.avatar).into(verificationBinding.imageView);
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {

            }
        });
    }

    private void getSignature() {
        AlertDialog alertDialog = new AlertDialog.Builder(Verification.this).create();
        final SignaturePad signaturePad = new SignaturePad(getApplicationContext(), null);
        alertDialog.setView(signaturePad);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.complete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                signatureImage = signaturePad.getSignatureBitmap();
                verificationBinding.signatureSet.setImageBitmap(signatureImage);

                bs = new ByteArrayOutputStream();
                signatureImage.compress(Bitmap.CompressFormat.PNG, 100, bs);

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
            }
        });
        alertDialog.setTitle("Signature");
        alertDialog.show();
    }

    private void captureImageUsingCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.i("TAG", "IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d("TAG", "Image path: " + mCurrentPhotoPath);
        return image;
    }

    public void fromCamera() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                //mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(mCurrentPhotoPath));
                mImageBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                //mImageBitmap = Bitmap.createScaledBitmap(mImageBitmap, mImageBitmap.getWidth() / 5, mImageBitmap.getHeight() / 5, true);
                verificationBinding.verificationCaptureImage.setImageBitmap(mImageBitmap);
                submitModel.setPhoto(mCurrentPhotoPath);

                MediaStore.Images.Media.insertImage(getContentResolver(),
                        image.getAbsolutePath(), image.getName(), null);
                sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(image)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK) {
            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                verificationBinding.verificationCaptureImage.setImageBitmap(mImageBitmap);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
                submitModel.setPhoto(imageFileName);
                //Log.d("TAG", "Photo target: " + submitTaskModel.getPhoto());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }
}
