package com.sslwireless.biddyut.rider.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.bumptech.glide.Glide;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.SummaryBinding;
import com.sslwireless.biddyut.rider.model.basic.SubmitTaskModel;
import com.sslwireless.biddyut.rider.model.response.stats.StatisticsModel;

/**
 * Created by Asif Malik on 7/20/2017.
 */

public class Summary extends AppCompatActivity {

    private SummaryBinding summaryBinding;
    private StatisticsModel statisticsModel;
    private Typeface roboto_condence;
    private Context context;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        summaryBinding = DataBindingUtil.setContentView(this, R.layout.summary);
        statisticsModel = (StatisticsModel) getIntent().getSerializableExtra("stats");
        summaryBinding.setSummaryData(statisticsModel);

        context = this;
        Glide.with(context).load(R.drawable.avatar).into(summaryBinding.imageView);

        toolbar = (Toolbar) findViewById(R.id.include2);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");

        summaryBinding.summaryTaskText.setVisibility(View.GONE);

        if (statisticsModel != null) {
            String startTime = statisticsModel.getResponse().getStartTime();
            String dateSplitter[] = startTime.split(" ");
            String deliveryStartTime = dateSplitter[1];
            summaryBinding.dStartTimeText.setText(deliveryStartTime);

            String endTime = statisticsModel.getResponse().getEndTime();
            String dateSplitter1[] = endTime.split(" ");
            String deliveryEndTime = dateSplitter1[1];
            summaryBinding.dEndTimeText.setText(deliveryEndTime);

            summaryBinding.dTimeText.setText(statisticsModel.getResponse().getSpend());
        }

        summaryBinding.homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        initTypeface();
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                return true;
            *//*default:
                return super.onOptionsItemSelected(item);*//*
        }
        return super.onOptionsItemSelected(item);
    }*/


    private void initTypeface() {
        summaryBinding.textView3.setTypeface(roboto_condence);
        summaryBinding.textView4.setTypeface(roboto_condence);
        //summaryBinding.textView5.setTypeface(roboto_condence);
        summaryBinding.deliveryStartTime.setTypeface(roboto_condence);
        summaryBinding.deliveryEndTime.setTypeface(roboto_condence);
        summaryBinding.deliveryTime.setTypeface(roboto_condence);
        summaryBinding.dStartTimeText.setTypeface(roboto_condence);
        summaryBinding.dEndTimeText.setTypeface(roboto_condence);
        summaryBinding.dTimeText.setTypeface(roboto_condence);
        summaryBinding.verificationTitle.setTypeface(roboto_condence);
        summaryBinding.homeBtn.setTypeface(roboto_condence);
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);*/
    }
}
