package com.sslwireless.biddyut.rider.model.basic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sslwireless.biddyut.rider.model.response.details.Product;
import com.sslwireless.biddyut.rider.model.response.stats.StatisticsModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by asif.malik on 8/17/2017.
 */

public class SubmitTaskModel implements Serializable {
    @SerializedName("task_id")
    @Expose
    private Integer task_id;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reason_id")
    @Expose
    private String reason_id;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("amount")
    @Expose
    private String amount;

    public SubmitTaskModel() {

    }

    public SubmitTaskModel(Integer task_id, Integer quantity, String status) {
        this.task_id = task_id;
        this.quantity = quantity;
        this.status = status;
    }

    public SubmitTaskModel(Integer task_id, Integer quantity, String status, String reason_id, String remarks, String amount) {
        this.task_id = task_id;
        this.quantity = quantity;
        this.status = status;
        this.reason_id = reason_id;
        this.remarks = remarks;
        this.amount = amount;
    }

    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason_id() {
        return reason_id;
    }

    public void setReason_id(String reason_id) {
        this.reason_id = reason_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
