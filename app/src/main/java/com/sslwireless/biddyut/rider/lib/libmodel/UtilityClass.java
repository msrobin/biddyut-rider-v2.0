package com.sslwireless.biddyut.rider.lib.libmodel;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;

/**
 * Created by SSL_ZAHID on 4/17/2016.
 */
public class UtilityClass {

    public static void goNextPage(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    public static void goPreviousPage(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.activity_in_back, R.anim.activity_out_back);
    }

    public static void showToast(Context context, String messageForToast, int timeToShow) {
        Toast.makeText(context, messageForToast, timeToShow).show();
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
