package com.sslwireless.biddyut.rider.lib.libmodel;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by SSL_ZAHID on 4/2/2017.
 */
public class StoreInfo {

    private static SharedPreferences getSharePref(Context context){
        return context.getSharedPreferences("local_store",1);
    }

    public static void setData(Context context, String key, String value){
        getSharePref(context).edit().putString(key,value).commit();
    }

    public static void removeData(Context context, String key){
        getSharePref(context).edit().remove(key).commit();
    }

    public static String getData(Context context, String key){
        return getSharePref(context).getString(key,null);
    }
}
