
package com.sslwireless.biddyut.rider.model.response.task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Task implements Serializable {

    @SerializedName("task_group_id")
    @Expose
    private Integer taskGroupId;
    @SerializedName("task_type")
    @Expose
    private String task_type;
    @SerializedName("task_title")
    @Expose
    private String taskTitle;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("task_latitude")
    @Expose
    private String taskLatitude;
    @SerializedName("task_longitude")
    @Expose
    private String taskLongitude;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("consignment_unique_id")
    @Expose
    private String consignment_unique_id;
    @SerializedName("task_status")
    @Expose
    private String taskStatus;

    public String getConsignment_unique_id() {
        return consignment_unique_id;
    }

    public void setConsignment_unique_id(String consignment_unique_id) {
        this.consignment_unique_id = consignment_unique_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTask_type() {
        return task_type;
    }

    public void setTask_type(String task_type) {
        this.task_type = task_type;
    }

    public Integer getTaskGroupId() {
        return taskGroupId;
    }

    public void setTaskGroupId(Integer taskGroupId) {
        this.taskGroupId = taskGroupId;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskLatitude() {
        return taskLatitude;
    }

    public void setTaskLatitude(String taskLatitude) {
        this.taskLatitude = taskLatitude;
    }

    public String getTaskLongitude() {
        return taskLongitude;
    }

    public void setTaskLongitude(String taskLongitude) {
        this.taskLongitude = taskLongitude;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

}
