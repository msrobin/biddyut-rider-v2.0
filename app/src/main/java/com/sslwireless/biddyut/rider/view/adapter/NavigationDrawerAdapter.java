package com.sslwireless.biddyut.rider.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.model.basic.Information;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by asif.malik on 8/27/2017.
 */

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    List<Information> data = Collections.emptyList();
    private ClickListener clickListener;
    LinearLayout ll;
    int header_type = 0, item_type = 1;
    private Typeface roboto_condence;
    private String pic, name, mobile, email;

    public NavigationDrawerAdapter(Context context, List<Information> data) {
        inflater = LayoutInflater.from(context);
        this.data = data;
        getUserData(inflater.getContext());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == header_type) {
            View view = inflater.inflate(R.layout.custom_header, parent, false);
            MyViewHolder holder = new MyViewHolder(view, viewType);
            return holder;
        } else if (viewType == item_type) {
            View view = inflater.inflate(R.layout.custom_navigation_recycler, parent, false);
            MyViewHolder holder = new MyViewHolder(view, viewType);
            return holder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (holder.Holderid == 2) {
            Information current = data.get(position);
            holder.title.setText(current.title);
            holder.icon.setImageResource(current.itemImage);
        } else if (holder.Holderid == 1) {
            Glide.with(inflater.getContext()).load(pic).into(holder.user_image);
            holder.sign_in_user_name.setText(name);
            holder.sign_in_user_mobile.setText(mobile);
            holder.sign_in_user_email.setText(email);
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title, sign_in_user_name, sign_in_user_mobile, sign_in_user_email;
        ImageView icon;
        int Holderid;
        CircleImageView user_image;

        public MyViewHolder(View itemView, int viewType) {
            super(itemView);
            roboto_condence = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/RobotoCondensed-Bold.ttf");
            if (viewType == header_type) {
                Holderid = 1;
                user_image = (CircleImageView) itemView.findViewById(R.id.user_image);
                sign_in_user_name = (TextView) itemView.findViewById(R.id.sign_in_user_name);
                sign_in_user_mobile = (TextView) itemView.findViewById(R.id.sign_in_user_mobile);
                sign_in_user_email = (TextView) itemView.findViewById(R.id.sign_in_user_email);

                sign_in_user_name.setTypeface(roboto_condence);
                sign_in_user_mobile.setTypeface(roboto_condence);
                sign_in_user_email.setTypeface(roboto_condence);
            } else if (viewType == item_type) {
                itemView.setOnClickListener(this);
                ll = (LinearLayout) itemView.findViewById(R.id.ll);
                title = (TextView) itemView.findViewById(R.id.tv);
                icon = (ImageView) itemView.findViewById(R.id.iv);
                Holderid = 2;
                title.setTypeface(roboto_condence);
            }
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.itemClicked(v, getPosition());
            }
        }
    }

    public interface ClickListener {

        public void itemClicked(View v, int position);
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return header_type;
        return item_type;
    }

    public void getUserData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        pic = preferences.getString("pic", "null");
        name = preferences.getString("name", "null");
        mobile = preferences.getString("mobile", "null");
        email = preferences.getString("email", "null");
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }
}