
package com.sslwireless.biddyut.rider.model.response.task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Consignment implements Serializable {

    @SerializedName("consignment_unique_id")
    @Expose
    private String consignmentUniqueId;
    @SerializedName("consignment_type")
    @Expose
    private String consignmentType;
    @SerializedName("consignment_status")
    @Expose
    private String consignmentStatus;

    public String getConsignmentUniqueId() {
        return consignmentUniqueId;
    }

    public void setConsignmentUniqueId(String consignmentUniqueId) {
        this.consignmentUniqueId = consignmentUniqueId;
    }

    public String getConsignmentType() {
        return consignmentType;
    }

    public void setConsignmentType(String consignmentType) {
        this.consignmentType = consignmentType;
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

}
