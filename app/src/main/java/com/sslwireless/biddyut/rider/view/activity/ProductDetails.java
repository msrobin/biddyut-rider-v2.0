package com.sslwireless.biddyut.rider.view.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.icu.text.LocaleDisplayNames;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.ProductDetailsBinding;
import com.sslwireless.biddyut.rider.model.basic.BasicModel;
import com.sslwireless.biddyut.rider.model.basic.SubmitModel;
import com.sslwireless.biddyut.rider.model.basic.SubmitTaskModel;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.adapter.TaskDetailsAdapter;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Asif Malik on 7/18/2017.
 */

public class ProductDetails extends AppCompatActivity {

    private ProductDetailsBinding productDetailsBinding;
    private Typeface roboto_condence;
    private Context context;
    private Toolbar toolbar;
    private String consignment_id, token;
    private int task_group_id, qunty;
    private ApiInterface apiInterface;
    private TaskDetails taskDetails;
    private TaskDetailsAdapter adapter;
    private TaskList taskList;
    private ProgressDialog dialogs;
    private SubmitTaskModel submitTaskModel;
    private SubmitModel submitModel;
    private float total_payable_price, dCharge, unit_product_price, quantity;
    private ArrayList<SubmitTaskModel> submitTaskModelList = new ArrayList<SubmitTaskModel>();
    private int flag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productDetailsBinding = DataBindingUtil.setContentView(this, R.layout.product_details);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.product_details_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressD();

        token = getIntent().getStringExtra("token");
        consignment_id = getIntent().getStringExtra("consignment_unique_id");
        task_group_id = getIntent().getIntExtra("task_group_id", 0);
        taskList = (TaskList) getIntent().getSerializableExtra("task_list");
        submitTaskModel = (SubmitTaskModel) getIntent().getSerializableExtra("submit_task_model");
        submitModel = (SubmitModel) getIntent().getSerializableExtra("submit_task");
        submitTaskModelList = (ArrayList<SubmitTaskModel>) getIntent().getSerializableExtra("submit_task_model_list");

        Log.d("TAG", "token: " + token);
        Log.d("TAG", "consignment_id: " + consignment_id);
        Log.d("TAG", "task_group_id: " + task_group_id);
        Log.d("TAG", "submitTaskModelList: " + submitTaskModelList.size());

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");

        initTypeface();

        productDetailsBinding.productDetailsProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToVerification();
            }
        });

        if (taskList.getResponse().getConsignment().getConsignmentType().equals("picking")) {
            productDetailsBinding.linear3.setVisibility(View.GONE);
            productDetailsBinding.view.setVisibility(View.GONE);
            productDetailsBinding.calculationLayout.setVisibility(View.GONE);
        }
        getTaskDetails();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom_message"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver2,
                new IntentFilter("custom_message_2"));
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String qty = intent.getStringExtra("quantity");
            qunty = Integer.parseInt(qty);

            flag = intent.getIntExtra("alert", 0);

            float product_result = qunty * unit_product_price;
            productDetailsBinding.productPrice.setText(String.valueOf(product_result) + " BDT");

            total_payable_price = product_result + dCharge;
            productDetailsBinding.payablePrice.setText(total_payable_price + " BDT");

            if (productDetailsBinding.calculationLayout.getVisibility() == View.VISIBLE) {
                String endTime = productDetailsBinding.payablePrice.getText().toString();
                String dateSplitter1[] = endTime.split(" ");
                String deliveryEndTime = dateSplitter1[0];
                submitTaskModelList.get(0).setAmount(deliveryEndTime);
            }
        }
    };

    public BroadcastReceiver mMessageReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            flag = intent.getIntExtra("alert", 0);
        }
    };

    private void initTypeface() {
        productDetailsBinding.productDetailsTitle.setTypeface(roboto_condence);
        productDetailsBinding.productDetailsProceed.setTypeface(roboto_condence);
        productDetailsBinding.productDetailsNameOfProduct.setTypeface(roboto_condence);
        productDetailsBinding.productDetailsQuantity.setTypeface(roboto_condence);
        productDetailsBinding.productDetailsCost.setTypeface(roboto_condence);
        productDetailsBinding.productDetailsAmount.setTypeface(roboto_condence);
        productDetailsBinding.pPrice.setTypeface(roboto_condence);
        productDetailsBinding.payPrice.setTypeface(roboto_condence);
        productDetailsBinding.dPrice.setTypeface(roboto_condence);
        productDetailsBinding.productPrice.setTypeface(roboto_condence);
        productDetailsBinding.deliveryCharge.setTypeface(roboto_condence);
        productDetailsBinding.payablePrice.setTypeface(roboto_condence);
    }

    private void goToVerification() {
        if (flag == 1) {
            Toast.makeText(context, "You must select reason", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(ProductDetails.this, Verification.class);
            intent.putExtra("token", token);
            intent.putExtra("consignment_unique_id", consignment_id);
            intent.putExtra("task_group_id", task_group_id);
            intent.putExtra("task_list", taskList);
            intent.putExtra("submit_task", submitModel);
            intent.putExtra("submit_task_model", submitTaskModel);
            intent.putExtra("submit_task_model_list", submitTaskModelList);
            startActivity(intent);
        }
    }

    private void getTaskDetails() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskDetails> call = apiInterface.getTaskDetails(token, consignment_id, String.valueOf(task_group_id));
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                dialogs.dismiss();
                taskDetails = response.body();
                productDetailsBinding.productDetailsRecycler.setHasFixedSize(true);
                adapter = new TaskDetailsAdapter(context, taskDetails.getResponse().getProducts(), submitTaskModelList, submitModel, taskList);
                productDetailsBinding.productDetailsRecycler.setLayoutManager(new LinearLayoutManager(context));
                productDetailsBinding.productDetailsRecycler.setAdapter(adapter);

                quantity = taskDetails.getResponse().getProducts().get(0).getQuantity();
                unit_product_price = Float.parseFloat(taskDetails.getResponse().getProducts().get(0).getUnitProductPrice());

                float product_result = quantity * unit_product_price;
                productDetailsBinding.productPrice.setText(String.valueOf(product_result) + " BDT");

                if (taskDetails.getResponse().getProducts().get(0).getDeliveryPayByCus().equals(1)) {
                    productDetailsBinding.deliveryCharge.setText(taskDetails.getResponse().getProducts().get(0).getTotalDeliveryCharge() + " BDT");
                    dCharge = Integer.parseInt(taskDetails.getResponse().getProducts().get(0).getTotalDeliveryCharge());
                } else {
                    productDetailsBinding.deliveryCharge.setText(taskDetails.getResponse().getProducts().get(0).getTotalDeliveryCharge() + " BDT");
                    dCharge = 0;
                }

                total_payable_price = product_result + dCharge;
                productDetailsBinding.payablePrice.setText(total_payable_price + " BDT");
                submitTaskModel.setAmount(String.valueOf(total_payable_price));
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {

            }
        });
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
