
package com.sslwireless.biddyut.rider.model.response.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Merchant implements Serializable {

    @SerializedName("merchant_name")
    @Expose
    private String merchantName;
    @SerializedName("merchant_email")
    @Expose
    private String merchantEmail;
    @SerializedName("merchant_msisdn")
    @Expose
    private String merchantMsisdn;
    @SerializedName("merchant_alt_msisdn")
    @Expose
    private String merchantAltMsisdn;

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getMerchantMsisdn() {
        return merchantMsisdn;
    }

    public void setMerchantMsisdn(String merchantMsisdn) {
        this.merchantMsisdn = merchantMsisdn;
    }

    public String getMerchantAltMsisdn() {
        return merchantAltMsisdn;
    }

    public void setMerchantAltMsisdn(String merchantAltMsisdn) {
        this.merchantAltMsisdn = merchantAltMsisdn;
    }

}
