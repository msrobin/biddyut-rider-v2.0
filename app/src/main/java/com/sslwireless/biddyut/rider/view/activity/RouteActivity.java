package com.sslwireless.biddyut.rider.view.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.RouteBinding;
import com.sslwireless.biddyut.rider.model.response.consignments.Route;
import com.sslwireless.biddyut.rider.model.utils.GoogleDirection;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asif.malik on 9/10/2017.
 */

public class RouteActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private Context context;
    private List<Marker> mMarkers = new ArrayList<Marker>();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private int flag = 0, gpsCounter = 0, timecounter = 0;
    private LatLng loc;
    private float currentZoom = -1;
    private GoogleDirection gd;
    Document mDoc;
    private RouteBinding routeBinding;
    private Toolbar toolbar;
    private ArrayList<Route> route;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        routeBinding = DataBindingUtil.setContentView(this, R.layout.activity_route);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.route_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        route = (ArrayList<Route>) getIntent().getSerializableExtra("consignment_route");
        Log.d("TAG", "Route size: " + route.size());

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.route_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkPermission()) {
            isGpsEnable();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermission();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flag == 1) {
            mapFragment.getMapAsync(this);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        }
    }

    private void GetMyLocation() {
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
    }

    GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {

        @Override
        public void onMyLocationChange(android.location.Location location) {
            if (gpsCounter == 0) {
                gpsCounter = 1;
                loc = new LatLng(location.getLatitude(), location.getLongitude());
                Log.d("TAG", "My current Position: " + loc);

                int[] color = getResources().getIntArray(R.array.rainbow);

                for (int i = 0; i < route.size(); i++) {
                    int val = color[i];
                    Log.d("TAG", "Color code: " + val);
                    if (!route.get(i).getLat().equals("")) {
                        if (i > 0)
                            i--;
                        LatLng start = new LatLng(Double.parseDouble(route.get(i).getLat()), Double.parseDouble(route.get(i).getLon()));
                        i++;
                        if (!route.get(i).getLon().equals("")) {
                            LatLng end = new LatLng(Double.parseDouble(route.get(i).getLat()), Double.parseDouble(route.get(i).getLon()));
                            CalculateRoute(start, end, val);
                        }
                    }
                }

                if (mMap != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15f));
                }
            }
        }
    };

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText(context, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGpsEnable();
                } else {
                    Toast.makeText(context, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void isGpsEnable() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            GetMyLocation();
            mMap.setOnCameraChangeListener(getCameraChangeListener());
        } else {
            showGPSDisabledAlertToUser();
        }
    }

    private void showNetworkAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RouteActivity.this);
        alertDialogBuilder.setMessage("Please Enable Your Network Connection First...").setTitle("Network Connecting Issue")
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Biddyut Rider");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Location service disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Go To Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                flag = 1;
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public GoogleMap.OnCameraChangeListener getCameraChangeListener() {
        return new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                if (currentZoom != position.zoom) {
                    {
                        currentZoom = position.zoom;
                        if (timecounter == 1) {
                            if (currentZoom < 12) {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(false);
                                }
                            } else {
                                for (int i = 0; i < mMarkers.size(); i++) {
                                    mMarkers.get(i).setVisible(true);
                                }
                            }
                        }
                    }
                }
            }

        };
    }

    public void CalculateRoute(final LatLng loc, final LatLng result, final int val) {
        gd = new GoogleDirection(context);

        gd.setOnDirectionResponseListener(new GoogleDirection.OnDirectionResponseListener() {
            public void onResponse(String status, Document doc, GoogleDirection gd) {

                mDoc = doc;

                mMap.addMarker(new MarkerOptions().position(loc)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_alt)));

                mMap.addMarker(new MarkerOptions().position(result)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_alt)));

                ArrayList<LatLng> directionPoint = gd.getDirection(doc);
                PolylineOptions rectLine = null;
                rectLine = new PolylineOptions().width(10).color(val);

                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }
                Polyline polylin = mMap.addPolyline(rectLine);
            }
        });
        gd.request(loc, result, GoogleDirection.MODE_DRIVING);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            /*default:
                return super.onOptionsItemSelected(item);*/
        }
        return super.onOptionsItemSelected(item);
    }
}
