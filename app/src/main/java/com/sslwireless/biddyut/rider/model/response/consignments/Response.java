package com.sslwireless.biddyut.rider.model.response.consignments;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response implements Serializable {

    @SerializedName("consignments")
    @Expose
    private ArrayList<Consignment> consignments = null;

    public ArrayList<Consignment> getConsignments() {
        return consignments;
    }

    public void setConsignments(ArrayList<Consignment> consignments) {
        this.consignments = consignments;
    }

}
