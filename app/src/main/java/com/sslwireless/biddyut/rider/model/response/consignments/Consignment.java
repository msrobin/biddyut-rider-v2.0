package com.sslwireless.biddyut.rider.model.response.consignments;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Consignment implements Serializable {

    @SerializedName("consignment_unique_id")
    @Expose
    private String consignmentUniqueId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("requested_amount")
    @Expose
    private Integer requestedAmount;
    @SerializedName("collected_amount")
    @Expose
    private Integer collectedAmount;
    @SerializedName("requested_quantity")
    @Expose
    private Integer requestedQuantity;
    @SerializedName("success_quantity")
    @Expose
    private Integer successQuantity;
    @SerializedName("return_quantity")
    @Expose
    private Integer returnQuantity;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("route")
    @Expose
    private ArrayList<Route> route = null;

    public String getConsignmentUniqueId() {
        return consignmentUniqueId;
    }

    public void setConsignmentUniqueId(String consignmentUniqueId) {
        this.consignmentUniqueId = consignmentUniqueId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(Integer requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public Integer getCollectedAmount() {
        return collectedAmount;
    }

    public void setCollectedAmount(Integer collectedAmount) {
        this.collectedAmount = collectedAmount;
    }

    public Integer getRequestedQuantity() {
        return requestedQuantity;
    }

    public void setRequestedQuantity(Integer requestedQuantity) {
        this.requestedQuantity = requestedQuantity;
    }

    public Integer getSuccessQuantity() {
        return successQuantity;
    }

    public void setSuccessQuantity(Integer successQuantity) {
        this.successQuantity = successQuantity;
    }

    public Integer getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(Integer returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public ArrayList<Route> getRoute() {
        return route;
    }

    public void setRoute(ArrayList<Route> route) {
        this.route = route;
    }

}
