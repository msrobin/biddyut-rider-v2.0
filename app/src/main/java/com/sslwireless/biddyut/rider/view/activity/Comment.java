package com.sslwireless.biddyut.rider.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.CommentsBinding;
import com.sslwireless.biddyut.rider.model.basic.SubmitModel;
import com.sslwireless.biddyut.rider.model.basic.SubmitTaskModel;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;
import com.sslwireless.biddyut.rider.model.response.reason.StatusReason;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.radius;

/**
 * Created by Asif Malik on 7/19/2017.
 */

public class Comment extends AppCompatActivity {

    CommentsBinding commentsBinding;
    String[] item = {"Success", "Partial", "Fail"};
    private String apiToken;
    private Context context;
    private ApiInterface apiInterface;
    private StatusReason statusReason;
    private ArrayList<String> reasonOfFailure = new ArrayList<String>();
    ArrayList<SubmitTaskModel> submitTaskModelList = new ArrayList<SubmitTaskModel>();
    private String deliveryStatusSelectedDropdown, failureSelectedDropdown;
    private String consignment_id, token, x;
    private int task_group_id, reason_id;
    private TaskList tasks;
    private SubmitModel submitModel;
    private SubmitTaskModel submitTaskModel;
    private TaskDetails taskDetails;
    private Typeface roboto_condence;
    private Toolbar toolbar;
    private ProgressDialog dialogs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commentsBinding = DataBindingUtil.setContentView(this, R.layout.comment);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.comment_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        initTypeFace();

        getApiToken(context);
        taskDetails = new TaskDetails();

        token = getIntent().getStringExtra("token");
        consignment_id = getIntent().getStringExtra("consignment_unique_id");
        task_group_id = getIntent().getIntExtra("task_group_id", 0);
        tasks = (TaskList) getIntent().getSerializableExtra("task_list");
        submitModel = (SubmitModel) getIntent().getSerializableExtra("submit_task");

        Log.d("TAG", "token: " + token);
        Log.d("TAG", "consignment_id: " + consignment_id);
        Log.d("TAG", "task_group_id: " + task_group_id);

        progressD();
        getTaskDetails();
        getStatusReason();
        commentsBinding.commentProceedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (commentsBinding.commentSpinner1.getSelectedItem() == null) {
                    Toast.makeText(context, "No data found", Toast.LENGTH_LONG).show();
                } else {
                    if (deliveryStatusSelectedDropdown.matches("")) {
                        Toast.makeText(context, "Please select delivery status", Toast.LENGTH_LONG).show();
                    } else if (deliveryStatusSelectedDropdown.equals("Success")) {
                        forSuccess();
                    } else if (deliveryStatusSelectedDropdown.equals("Fail")) {
                        forFail();
                    } else {
                        forPartial();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            /*default:
                return super.onOptionsItemSelected(item);*/
        }
        return super.onOptionsItemSelected(item);
    }

    private void forPartial() {
        submitTaskModel = new SubmitTaskModel();
        submitTaskModel.setStatus("partial");
        Intent intent = new Intent(Comment.this, ProductDetails.class);
        intent.putExtra("token", apiToken);
        intent.putExtra("consignment_unique_id", consignment_id);
        intent.putExtra("task_group_id", task_group_id);
        intent.putExtra("task_list", tasks);
        intent.putExtra("submit_task", submitModel);
        intent.putExtra("submit_task_model", submitTaskModel);
        intent.putExtra("submit_task_model_list", submitTaskModelList);
        startActivity(intent);
    }

    private void forFail() {
        if (/*!commentsBinding.commentAdditionalText.getText().toString().isEmpty() &&*/ !failureSelectedDropdown.equals("")) {
            for (int i = 0; i < taskDetails.getResponse().getProducts().size(); i++) {
                submitTaskModelList.add(new SubmitTaskModel(taskDetails.getResponse().getProducts().get(i).getTaskId(),
                        0, "fail", String.valueOf(reason_id),
                        commentsBinding.commentAdditionalText.getText().toString(), ""));
            }
            Intent intent = new Intent(Comment.this, Verification.class);
            intent.putExtra("token", apiToken);
            intent.putExtra("consignment_unique_id", consignment_id);
            intent.putExtra("task_group_id", task_group_id);
            intent.putExtra("submit_task", submitModel);
            intent.putExtra("submit_task_model", submitTaskModel);
            intent.putExtra("task_list", tasks);
            intent.putExtra("submit_task_model_list", submitTaskModelList);
            startActivity(intent);
        } else {
            Toast.makeText(context, "Please select reason", Toast.LENGTH_LONG).show();
        }
    }

    private void forSuccess() {
        if (tasks.getResponse().getConsignment().getConsignmentType().equals("picking")) {
            forPicking();
        } else {
            forDelivery();
        }
        Intent intent = new Intent(Comment.this, Verification.class);
        intent.putExtra("token", apiToken);
        intent.putExtra("consignment_unique_id", consignment_id);
        intent.putExtra("task_group_id", task_group_id);
        intent.putExtra("submit_task", submitModel);
        intent.putExtra("submit_task_model", submitTaskModel);
        intent.putExtra("task_list", tasks);
        intent.putExtra("submit_task_model_list", submitTaskModelList);
        startActivity(intent);
    }

    private void forDelivery() {
        String endTime = commentsBinding.deliveryCharge.getText().toString();
        String dateSplitter1[] = endTime.split(" ");
        String deliveryEndTime = dateSplitter1[0];

        if (!commentsBinding.commentAdditionalText.getText().toString().isEmpty()) {
            for (int i = 0; i < taskDetails.getResponse().getProducts().size(); i++) {
                submitTaskModelList.add(new SubmitTaskModel(taskDetails.getResponse().getProducts().get(i).getTaskId(),
                        taskDetails.getResponse().getProducts().get(i).getQuantity(),
                        "full", "", commentsBinding.commentAdditionalText.getText().toString(), deliveryEndTime));
            }
        } else {
            for (int i = 0; i < taskDetails.getResponse().getProducts().size(); i++) {
                submitTaskModelList.add(new SubmitTaskModel(taskDetails.getResponse().getProducts().get(i).getTaskId(),
                        taskDetails.getResponse().getProducts().get(i).getQuantity(),
                        "full", "", "", deliveryEndTime));
            }
        }
    }

    private void forPicking() {
        if (!commentsBinding.commentAdditionalText.getText().toString().isEmpty()) {
            for (int i = 0; i < taskDetails.getResponse().getProducts().size(); i++) {
                submitTaskModelList.add(new SubmitTaskModel(taskDetails.getResponse().getProducts().get(i).getTaskId(),
                        taskDetails.getResponse().getProducts().get(i).getQuantity(),
                        "full", "", commentsBinding.commentAdditionalText.getText().toString(), ""));
            }
        } else {
            for (int i = 0; i < taskDetails.getResponse().getProducts().size(); i++) {
                submitTaskModelList.add(new SubmitTaskModel(taskDetails.getResponse().getProducts().get(i).getTaskId(),
                        taskDetails.getResponse().getProducts().get(i).getQuantity(),
                        "full", "", "", ""));
            }
        }
    }

    private void initTypeFace() {
        commentsBinding.commentsTitle.setTypeface(roboto_condence);
        commentsBinding.commentsProceed.setTypeface(roboto_condence);
        commentsBinding.textView.setTypeface(roboto_condence);
        commentsBinding.reasonSpinnerText.setTypeface(roboto_condence);
        commentsBinding.commentAdditional.setTypeface(roboto_condence);
        commentsBinding.commentAdditionalText.setTypeface(roboto_condence);
        commentsBinding.deliveryCharge.setTypeface(roboto_condence);
        commentsBinding.dPrice.setTypeface(roboto_condence);
    }

    private void getStatusReason() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusReason> call = apiInterface.statusReason(apiToken, tasks.getResponse().getConsignment().getConsignmentType());
        call.enqueue(new Callback<StatusReason>() {
            @Override
            public void onResponse(Call<StatusReason> call, Response<StatusReason> response) {
                statusReason = response.body();
                if (statusReason.getStatusCode() == 200 && statusReason.getStatus().equals("success")) {
                    setDataToSpinner();
                }
            }

            @Override
            public void onFailure(Call<StatusReason> call, Throwable t) {

            }
        });
    }

    private void setDataToSpinner() {
        reasonOfFailure.add("");

        for (int i = 0; i < statusReason.getResponse().size(); i++) {
            reasonOfFailure.add(statusReason.getResponse().get(i).getReason());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.custom_spinner,
                item
        ) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                ((TextView) v).setTypeface(externalFont);
                return v;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        commentsBinding.commentSpinner1.setAdapter(adapter);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(
                this,
                R.layout.custom_spinner,
                reasonOfFailure
        ) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                ((TextView) v).setTypeface(externalFont);
                return v;
            }
        };
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        commentsBinding.commentFailureSpinner.setAdapter(adapter1);

        commentsBinding.commentSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                deliveryStatusSelectedDropdown = adapterView.getItemAtPosition(i).toString();
                Log.d("TAG", "deliveryStatusSelectedDropdown: " + deliveryStatusSelectedDropdown);

                if (deliveryStatusSelectedDropdown.equals("Success")) {
                    commentsBinding.reasonSpinnerText.setVisibility(View.GONE);
                    commentsBinding.commentFailureSpinner.setVisibility(View.GONE);
                    commentsBinding.commentAdditional.setVisibility(View.VISIBLE);
                    commentsBinding.remarksLay.setVisibility(View.VISIBLE);
                    if (tasks.getResponse().getConsignment().getConsignmentType().equals("picking")) {
                        commentsBinding.commentPayableLay.setVisibility(View.GONE);
                    } else {
                        commentsBinding.commentPayableLay.setVisibility(View.VISIBLE);
                    }
                } else if (deliveryStatusSelectedDropdown.equals("Fail")) {
                    commentsBinding.reasonSpinnerText.setVisibility(View.VISIBLE);
                    commentsBinding.commentFailureSpinner.setVisibility(View.VISIBLE);
                    commentsBinding.commentAdditional.setVisibility(View.VISIBLE);
                    commentsBinding.remarksLay.setVisibility(View.VISIBLE);
                    commentsBinding.commentPayableLay.setVisibility(View.GONE);
                } else {
                    commentsBinding.reasonSpinnerText.setVisibility(View.GONE);
                    commentsBinding.commentFailureSpinner.setVisibility(View.GONE);
                    commentsBinding.commentAdditional.setVisibility(View.GONE);
                    commentsBinding.remarksLay.setVisibility(View.GONE);
                    commentsBinding.commentPayableLay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        commentsBinding.commentFailureSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                failureSelectedDropdown = adapterView.getItemAtPosition(i).toString();
                Log.d("TAG", "deliveryStatusSelectedDropdown: " + failureSelectedDropdown);

                for (int j = 0; j < statusReason.getResponse().size(); j++) {
                    x = statusReason.getResponse().get(j).getReason();

                    if (x.equals(failureSelectedDropdown)) {
                        reason_id = statusReason.getResponse().get(j).getId();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    private void getTaskDetails() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskDetails> call = apiInterface.getTaskDetails(token, consignment_id, String.valueOf(task_group_id));
        call.enqueue(new Callback<TaskDetails>() {
            @Override
            public void onResponse(Call<TaskDetails> call, Response<TaskDetails> response) {
                dialogs.dismiss();
                taskDetails = response.body();
                commentsBinding.deliveryCharge.setText(taskDetails.getResponse().getProducts().get(0).getTotalPayableAmount() + " BDT");
            }

            @Override
            public void onFailure(Call<TaskDetails> call, Throwable t) {
                Log.d("TAG", "Details failed message: " + t.toString());
            }
        });
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }
}
