package com.sslwireless.biddyut.rider.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.LoginBinding;
import com.sslwireless.biddyut.rider.model.response.login.LoginModel;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import io.fabric.sdk.android.Fabric;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Asif Malik on 7/23/2017.
 */

public class LoginActivity extends AppCompatActivity {

    private LoginBinding loginBinding;
    private Typeface fontAwsm;
    private Context context;
    private int flag = 0;
    private ApiInterface apiInterface;
    private LoginModel loginModel;
    private String isUserLogin;
    private ProgressDialog dialogs;
    private Typeface roboto_condence;
    private String apiToken, uPic, uName, uMobile, uEmail;
    ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        loginBinding = DataBindingUtil.setContentView(this, R.layout.login);
        context = this;

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        loginBinding.emailAddress.setTypeface(roboto_condence);
        loginBinding.password.setTypeface(roboto_condence);

        getIsLogin(context);
        getApiToken(context);
        getUserData(context);

        Crashlytics.setUserEmail(uEmail);

        if (isUserLogin.equals("1")) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            return;
        }

        fontAwsm = Typeface.createFromAsset(context.getAssets(), "fonts/FontAwesome.otf");

        loginBinding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = loginBinding.emailAddress.getText().toString();
                String password = loginBinding.password.getText().toString();

                if (email.matches("")) {
                    Toast.makeText(context, "Please enter email", Toast.LENGTH_LONG).show();
                } else if (password.matches("")) {
                    Toast.makeText(context, "Please enter password", Toast.LENGTH_LONG).show();
                } else {
                    if (!isEmailValid(email)) {
                        Toast.makeText(context, "Please enter valid email", Toast.LENGTH_LONG).show();
                    } else {
                        progressD();
                        loginCall(email, password);
                    }

                }
            }
        });

        loginBinding.passwordVisibility.setText(getResources().getString(R.string.password_invisible));
        loginBinding.passwordVisibility.setTypeface(fontAwsm);

        loginBinding.passwordVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag == 0) {
                    flag = 1;
                    loginBinding.passwordVisibility.setText(getResources().getString(R.string.password_visible));
                    loginBinding.passwordVisibility.setTypeface(fontAwsm);
                    loginBinding.password.setTransformationMethod(SingleLineTransformationMethod
                            .getInstance());
                } else {
                    flag = 0;
                    loginBinding.passwordVisibility.setText(getResources().getString(R.string.password_invisible));
                    loginBinding.passwordVisibility.setTypeface(fontAwsm);
                    loginBinding.password.setTransformationMethod(PasswordTransformationMethod
                            .getInstance());
                }

            }
        });

        loginBinding.login.setTypeface(roboto_condence);
    }

//    public void forceCrash(View view) {
//        throw new RuntimeException("This is a crash");
//    }


    private void loginCall(String email, String password) {
        final RequestBody email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email);
        final RequestBody password1 = RequestBody.create(MediaType.parse("multipart/form-data"), password);
        final RequestBody key = RequestBody.create(MediaType.parse("multipart/form-data"), "123456");

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<LoginModel> call = apiInterface.userLogin(email1, password1, key);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                loginModel = response.body();
                if (loginModel.getStatusCode() == 200 && loginModel.getStatus().equals("success")) {
                    dialogs.dismiss();
                    saveLoginStatus("1", context);
                    saveApiToken(loginModel.getResponse().getApiToken(), context);
                    saveUserData(loginModel.getResponse().getPhoto(), loginModel.getResponse().getName()
                            , loginModel.getResponse().getMsisdn(), loginModel.getResponse().getEmail(), context);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    //intent.putExtra("login_data", loginModel);
                    startActivity(intent);
                } else {
                    dialogs.dismiss();
                    Toast.makeText(context, loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dialogs.dismiss();
                Log.d("TAG", "Error message: " + t.toString());
            }
        });
    }

    private void saveApiToken(String apiToken, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("api_token", apiToken);
        editor.commit();
    }

    private void saveUserData(String pic, String name, String mobile, String email, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("pic", pic);
        editor.putString("name", name);
        editor.putString("mobile", mobile);
        editor.putString("email", email);
        editor.commit();
    }

    public void getUserData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        uPic = preferences.getString("pic", "null");
        uName = preferences.getString("name", "null");
        uMobile = preferences.getString("mobile", "null");
        uEmail = preferences.getString("email", "null");
    }

    private void saveLoginStatus(String loginStatus, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("is_login", loginStatus);
        editor.commit();
    }

    public String getIsLogin(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        isUserLogin = preferences.getString("is_login", "null");
        Log.d("TAG", "Is LoginActivity status: " + isUserLogin);
        return isUserLogin;
    }

    private static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }
}
