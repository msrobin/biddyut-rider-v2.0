package com.sslwireless.biddyut.rider.lib.libinterfaces;

/**
 * Created by SSL_ZAHID on 4/4/2017.
 */

public interface BasicItemOnClickListener {
    void getClickedPosition(int position);
}
