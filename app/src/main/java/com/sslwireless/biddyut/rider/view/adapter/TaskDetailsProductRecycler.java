package com.sslwireless.biddyut.rider.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.model.response.details.Product;
import com.sslwireless.biddyut.rider.model.response.details.TaskDetails;

import java.util.ArrayList;

/**
 * Created by asif.malik on 8/27/2017.
 */

public class TaskDetailsProductRecycler extends RecyclerView.Adapter<TaskDetailsProductRecycler.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Product> mProduct;
    private Context mContext;
    private ClickListener clickListener;
    private Typeface roboto_condence;

    public TaskDetailsProductRecycler(Context context, ArrayList<Product> product) {
        inflater = LayoutInflater.from(context);
        this.mProduct = product;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.task_details_product_recycler, parent, false);
        MyViewHolder holder = new MyViewHolder(view, viewType);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Product product = mProduct.get(position);
        holder.pickUpProductId.setText(product.getUniqueSuborderId());
        holder.pickUpProductTitle.setText(product.getTitle());
        holder.pickUpProductCategory.setText(product.getCategory());
        holder.pickUpProductQuantity.setText(String.valueOf(product.getQuantity()));
        holder.store_id_value.setText(String.valueOf(product.getStore_id()));
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return mProduct.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView pickUpProductTitle, pickUpProductCategory, pickUpProductQuantity,
                quantity, category, title, pickUpProductId, id, store_id, store_id_value;

        public MyViewHolder(View itemView, int viewType) {
            super(itemView);
            itemView.setOnClickListener(this);
            roboto_condence = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/RobotoCondensed-Bold.ttf");
            pickUpProductTitle = (TextView) itemView.findViewById(R.id.pickUpProductTitle);
            pickUpProductCategory = (TextView) itemView.findViewById(R.id.pickUpProductCategory);
            pickUpProductQuantity = (TextView) itemView.findViewById(R.id.pickUpProductQuantity);
            title = (TextView) itemView.findViewById(R.id.title);
            pickUpProductId = (TextView) itemView.findViewById(R.id.pickUpProductId);
            id = (TextView) itemView.findViewById(R.id.id);
            category = (TextView) itemView.findViewById(R.id.category);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            store_id = (TextView) itemView.findViewById(R.id.store_id);
            store_id_value = (TextView) itemView.findViewById(R.id.store_id_value);

            pickUpProductTitle.setTypeface(roboto_condence);
            pickUpProductCategory.setTypeface(roboto_condence);
            pickUpProductQuantity.setTypeface(roboto_condence);
            title.setTypeface(roboto_condence);
            category.setTypeface(roboto_condence);
            quantity.setTypeface(roboto_condence);
            id.setTypeface(roboto_condence);
            pickUpProductId.setTypeface(roboto_condence);
            store_id.setTypeface(roboto_condence);
            store_id_value.setTypeface(roboto_condence);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.itemClicked(v, getPosition());
            }
        }
    }

    public interface ClickListener {
        public void itemClicked(View v, int position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

