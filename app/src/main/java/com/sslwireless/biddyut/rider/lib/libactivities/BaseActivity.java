package com.sslwireless.biddyut.rider.lib.libactivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.lib.libinterfaces.PermissionListener;
import com.sslwireless.biddyut.rider.lib.libmodel.UtilityClass;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by SSL_ZAHID on 8/23/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private PermissionListener permissionListener;
    private static final int reqCode = 580;
    public DisplayMetrics displayMetrics = new DisplayMetrics();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        UtilityClass.goNextPage(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        viewRelatedTask();
    }

    public static String makeEncode(String value) {
        try {
            value = URLEncoder.encode(value, "utf-8");
        } catch (UnsupportedEncodingException e) {
        }
        return value;
    }

    public void showToast(String message) {
        showToast(message, false);
    }

    protected void showToast(String message, boolean isLong) {
        UtilityClass.showToast(getApplicationContext(), message, isLong ? Toast.LENGTH_LONG : Toast
                .LENGTH_SHORT);
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            UtilityClass.goPreviousPage(this);
        }catch (Exception e){}
    }

    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    protected abstract void viewRelatedTask();

    /**
     * Pass the list of permission for run time permission
     *
     * @param permission         All permission list. Example : Manifest.permission.WRITE_CALENDAR:
     * @param permissionListener Callback for permission listener.
     */
    protected void checkPermission(String[] permission, PermissionListener permissionListener) {
        try {
            this.permissionListener = permissionListener;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                boolean allGranted = true;
                int countPermission = 0;
                while (permission.length > countPermission) {
                    if ((ContextCompat.checkSelfPermission(this, permission[countPermission]) !=
                            PackageManager.PERMISSION_GRANTED)) {
                        allGranted = false;
                        break;
                    }
                    countPermission++;
                }
                if (!allGranted) ActivityCompat.requestPermissions(this, permission, reqCode);
                else this.permissionListener.permissionGranted();

            } else this.permissionListener.permissionGranted();

        } catch (Exception e) {
            this.permissionListener.permissionDenied(-1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        try {
            switch (requestCode) {
                case reqCode: {
                    if (permissions.length > 0) {
                        boolean allGranted = true;
                        int count = 0;
                        while (permissions.length > count) {
                            if (ContextCompat.checkSelfPermission(this, permissions[count]) !=
                                    PackageManager.PERMISSION_GRANTED) {
                                allGranted = false;
                                break;
                            }
                            count++;
                        }
                        if (allGranted) this.permissionListener.permissionGranted();
                        else this.permissionListener.permissionDenied(count);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager
                    .HIDE_NOT_ALWAYS);
        }
    }
}
