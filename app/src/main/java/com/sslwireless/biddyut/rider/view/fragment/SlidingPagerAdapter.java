package com.sslwireless.biddyut.rider.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.model.response.task.Consignment;
import com.sslwireless.biddyut.rider.model.response.task.Response;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.tabs.SlidingTabLayout;

import java.util.ArrayList;

/**
 * Created by Asif Malik on 7/18/2017.
 */

public class SlidingPagerAdapter extends FragmentStatePagerAdapter {

    private static final int List_Frag = 0;
    private static final int Map_Frag = 1;
    private LayoutInflater inflater;
    private int icons[] = {R.drawable.list_swipe_icon, R.drawable.map_swipe_icon};
    private String[] tabs;
    private Consignment consignments;
    private ArrayList<Task> taskLists;
    private TaskList tasks;
    private Fragment fragment = null;

    public SlidingPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        inflater = LayoutInflater.from(context);
        tabs = inflater.getContext().getResources().getStringArray(R.array.tabs);
        //tasks = taskList;
    }

    @Override
    public Fragment getItem(int position) {
        //Bundle bundle = new Bundle();
        switch (position) {
            case List_Frag:
                //bundle.putSerializable("task_list", tasks);
                fragment = TaskListFragment.newInstance();
                //fragment.setArguments(bundle);
                break;
            case Map_Frag:
                //bundle.putSerializable("task_list", tasks);
                fragment = MapFragment.newInstance();
                //fragment.setArguments(bundle);
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }

    @Override
    public int getCount() {
        return 2;
    }

    public int getDrawableData(int position) {
        return icons[position];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}
