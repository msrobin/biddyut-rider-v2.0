
package com.sslwireless.biddyut.rider.model.response.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Product implements Serializable {

    @SerializedName("task_id")
    @Expose
    private Integer taskId;
    @SerializedName("task_type")
    @Expose
    private String taskType;
    @SerializedName("unique_suborder_id")
    @Expose
    private String uniqueSuborderId;
    @SerializedName("merchant_order_id")
    @Expose
    private String merchantOrderId;
    @SerializedName("store_id")
    @Expose
    private String store_id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("unit_product_price")
    @Expose
    private String unitProductPrice;
    @SerializedName("unit_deivery_charge")
    @Expose
    private String unitDeiveryCharge;
    @SerializedName("total_product_price")
    @Expose
    private String totalProductPrice;
    @SerializedName("total_delivery_charge")
    @Expose
    private String totalDeliveryCharge;
    @SerializedName("payable_product_price")
    @Expose
    private String payableProductPrice;
    @SerializedName("total_payable_amount")
    @Expose
    private String totalPayableAmount;
    @SerializedName("delivery_pay_by_cus")
    @Expose
    private Integer deliveryPayByCus;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("distance")
    @Expose
    private Double distance;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getUniqueSuborderId() {
        return uniqueSuborderId;
    }

    public void setUniqueSuborderId(String uniqueSuborderId) {
        this.uniqueSuborderId = uniqueSuborderId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getUnitProductPrice() {
        return unitProductPrice;
    }

    public void setUnitProductPrice(String unitProductPrice) {
        this.unitProductPrice = unitProductPrice;
    }

    public String getUnitDeiveryCharge() {
        return unitDeiveryCharge;
    }

    public void setUnitDeiveryCharge(String unitDeiveryCharge) {
        this.unitDeiveryCharge = unitDeiveryCharge;
    }

    public String getTotalProductPrice() {
        return totalProductPrice;
    }

    public void setTotalProductPrice(String totalProductPrice) {
        this.totalProductPrice = totalProductPrice;
    }

    public String getTotalDeliveryCharge() {
        return totalDeliveryCharge;
    }

    public void setTotalDeliveryCharge(String totalDeliveryCharge) {
        this.totalDeliveryCharge = totalDeliveryCharge;
    }

    public String getPayableProductPrice() {
        return payableProductPrice;
    }

    public void setPayableProductPrice(String payableProductPrice) {
        this.payableProductPrice = payableProductPrice;
    }

    public String getTotalPayableAmount() {
        return totalPayableAmount;
    }

    public void setTotalPayableAmount(String totalPayableAmount) {
        this.totalPayableAmount = totalPayableAmount;
    }

    public Integer getDeliveryPayByCus() {
        return deliveryPayByCus;
    }

    public void setDeliveryPayByCus(Integer deliveryPayByCus) {
        this.deliveryPayByCus = deliveryPayByCus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

}
