package com.sslwireless.biddyut.rider.model.basic;

import java.io.Serializable;

/**
 * Created by asif.malik on 8/17/2017.
 */

public class SubmitModel implements Serializable {
    private double end_lat;
    private double end_lon;
    private String distance;
    private String photo;
    private String signature;
    private int temp_val;

    public SubmitModel() {

    }

    public int getTemp_val() {
        return temp_val;
    }

    public void setTemp_val(int temp_val) {
        this.temp_val = temp_val;
    }

    public double getEnd_lat() {
        return end_lat;
    }

    public void setEnd_lat(double end_lat) {
        this.end_lat = end_lat;
    }

    public double getEnd_lon() {
        return end_lon;
    }

    public void setEnd_lon(double end_lon) {
        this.end_lon = end_lon;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
