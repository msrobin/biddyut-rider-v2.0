package com.sslwireless.biddyut.rider.lib.libvalidation;

/**
 * Created by SSL_ZAHID on 8/23/2016.
 */
public class FormValidation {

    public static final int VALID = 0;
    public static final int INVALID = -1;
    public static final int FIELD_NULL = 1;
    public static final int FIELD_EMPTY = 2;
    public static final int PASSWORD_MISS_MATCH = 3;
    public static final int ERROR_PASSWORD_MIN_LENGTH = 4;

    /*Projects Static Data*/
    private static final int PASSWORD_MIN_LENGTH = 6;

    public static int notEmptyField(String fieldValue){
        try {
            if (fieldValue.toString().isEmpty()) return FIELD_EMPTY;
            else return VALID;
        }catch (Exception e){return FIELD_NULL;}
    }

    public static int isValidEmail(String emailAddress){
        if (emailAddress == null) {
            return FIELD_NULL;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches() ? VALID : INVALID;
        }
    }

    public static int isValidPassword(String password){
        try {
            if(password.length() < PASSWORD_MIN_LENGTH) return ERROR_PASSWORD_MIN_LENGTH;
            else return VALID;
        }catch (Exception e){
            return INVALID;
        }
    }

    public static int checkConfirmPassword(String password, String confirmPassword){
        try {
            if(!password.equals(confirmPassword)) return PASSWORD_MISS_MATCH;
            else return VALID;
        }catch (Exception e){
            return INVALID;
        }
    }

}
