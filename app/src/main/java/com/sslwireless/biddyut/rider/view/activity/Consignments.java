package com.sslwireless.biddyut.rider.view.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.ConsignmentsBinding;
import com.sslwireless.biddyut.rider.model.response.consignments.ConsignmentsModel;
import com.sslwireless.biddyut.rider.model.response.reconcilation.Reconcilation;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.adapter.ConsignmentsRecyclerAdapter;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asif.malik on 9/10/2017.
 */

public class Consignments extends AppCompatActivity implements ConsignmentsRecyclerAdapter.ClickListener {

    private ConsignmentsBinding consignmentsBinding;
    private Context context;
    private Toolbar toolbar;
    private ApiInterface apiInterface;
    private ConsignmentsModel consignmentsModel;
    private ConsignmentsRecyclerAdapter adapter;
    private ProgressDialog dialogs;
    private String apiToken;
    private Reconcilation reconcilation;
    private TaskList taskList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        consignmentsBinding = DataBindingUtil.setContentView(this, R.layout.activity_consignments);

        context = this;
        getApiToken(context);
        getTaskList(apiToken);

        toolbar = (Toolbar) findViewById(R.id.consignments_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressD();
    }

    private void getConsignmentsData() {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ConsignmentsModel> call = apiInterface.consignments(apiToken);
        call.enqueue(new Callback<ConsignmentsModel>() {
            @Override
            public void onResponse(Call<ConsignmentsModel> call, Response<ConsignmentsModel> response) {
                consignmentsModel = response.body();
                if (response.isSuccessful()) {
                    if (consignmentsModel.getStatusCode() == 200) {
                        dialogs.dismiss();
                        consignmentsBinding.consignmentsRecycler.setHasFixedSize(true);
                        adapter = new ConsignmentsRecyclerAdapter(context, consignmentsModel.getResponse().getConsignments());
                        consignmentsBinding.consignmentsRecycler.setLayoutManager(new LinearLayoutManager(context));
                        consignmentsBinding.consignmentsRecycler.setAdapter(adapter);
                        adapter.setClickListener(Consignments.this);
                    } else if (consignmentsModel.getStatusCode() == 204) {
                        dialogs.dismiss();
                        Toast.makeText(context, consignmentsModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConsignmentsModel> call, Throwable t) {
                dialogs.dismiss();
                Log.d("TAG", "Error message: " + t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    private void showReconsillationAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Biddyut Rider");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Do you want to make a request for reconciliation?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //flag = 1;
                                progressD();
                                submitReconRequest();
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void submitReconRequest() {
        if (taskList.getResponse() != null) {
            apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            Call<Reconcilation> call = apiInterface.reconRequest(apiToken, taskList.getResponse().getConsignment().getConsignmentUniqueId());
            call.enqueue(new Callback<Reconcilation>() {
                @Override
                public void onResponse(Call<Reconcilation> call, Response<Reconcilation> response) {
                    dialogs.dismiss();
                    reconcilation = response.body();
                    Toast.makeText(context, reconcilation.getMessage().get(0).toString(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent("get_task_api_call");
                    intent.putExtra("api_call", 1);
                    //intent.putExtra("status_code", taskList.getStatusCode());
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    getConsignmentsData();
                    //getTaskList(apiToken);
                    //flag = 1;
                    //taskList.getResponse().getConsignment().setConsignmentUniqueId(null);
                }

                @Override
                public void onFailure(Call<Reconcilation> call, Throwable t) {

                }
            });
        } else {
            dialogs.dismiss();
            Toast.makeText(context, taskList.getMessage().get(0).toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void itemClicked(View v, int position, String tag) {
        if (tag.equals("recon")) {
            showReconsillationAlertToUser();
        } else if (tag.equals("map")) {
            Intent intent = new Intent(Consignments.this, RouteActivity.class);
            intent.putExtra("consignment_route", consignmentsModel.getResponse().getConsignments().get(position).getRoute());
            startActivity(intent);
        }
    }

    private void getTaskList(String apiToken) {
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskList> call = apiInterface.getTaskList(apiToken);
        call.enqueue(new Callback<TaskList>() {
            @Override
            public void onResponse(Call<TaskList> call, Response<TaskList> response) {
                taskList = response.body();
                if (response.isSuccessful()) {
                    Log.d("TAG", "status code: " + taskList.getStatusCode());
                    if (taskList.getStatusCode() == 200) {
                        dialogs.dismiss();
                        getConsignmentsData();
                    } else {
                        dialogs.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskList> call, Throwable t) {
                dialogs.dismiss();
                Log.d("TAG", "Failed message: " + t.toString());
            }
        });
    }
}
