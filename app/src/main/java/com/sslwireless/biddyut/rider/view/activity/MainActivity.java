package com.sslwireless.biddyut.rider.view.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sslwireless.biddyut.rider.R;
import com.sslwireless.biddyut.rider.databinding.MainActivityBinding;
import com.sslwireless.biddyut.rider.model.response.login.LoginModel;
import com.sslwireless.biddyut.rider.model.response.reason.StatusReason;
import com.sslwireless.biddyut.rider.model.response.reconcilation.Reconcilation;
import com.sslwireless.biddyut.rider.model.response.task.Task;
import com.sslwireless.biddyut.rider.model.response.task.TaskList;
import com.sslwireless.biddyut.rider.view.fragment.MapFragment;
import com.sslwireless.biddyut.rider.view.fragment.NavigationDrawerFragment;
import com.sslwireless.biddyut.rider.view.fragment.SlidingPagerAdapter;
import com.sslwireless.biddyut.rider.view.service.RiderLocationService;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiClient;
import com.sslwireless.biddyut.rider.viewmodel.logic.network.ApiInterface;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity /*implements SwipeRefreshLayout.OnRefreshListener*/ {
    private Context context;
    private MainActivityBinding mainActivityBinding;
    private Typeface roboto_condence;
    private Toolbar toolbar;
    private String apiToken;
    private ApiInterface apiInterface;
    private TaskList taskList;
    private ProgressDialog dialogs;
    private int doneTaskCounter;
    private int counter, drawer_flag;
    private double totalDistance = 0;
    private SlidingPagerAdapter adapter = null;
    private LoginModel loginModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        context = this;
        progressD();
        getApiToken(context);
        getTaskList(apiToken);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        //loginModel = (LoginModel) getIntent().getSerializableExtra("login_data");

        roboto_condence = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        mainActivityBinding.retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressD();
                adapter = null;
                getTaskList(apiToken);
            }
        });
        mainActivityBinding.task.setTypeface(roboto_condence);
        mainActivityBinding.distanceTask.setTypeface(roboto_condence);
        mainActivityBinding.distanceTravelled.setTypeface(roboto_condence);
        mainActivityBinding.emptyIcon.setTypeface(roboto_condence);
        mainActivityBinding.retryButton.setTypeface(roboto_condence);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("get_task_api_call"));
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            counter = intent.getIntExtra("api_call", 0);
            if (counter == 1) {
                adapter = null;
                getApiToken(context);
                getTaskList(apiToken);
            }
        }
    };

    private void getTaskList(String apiToken) {
        taskList = null;
        totalDistance = 0;
        doneTaskCounter = 0;
        adapter = new SlidingPagerAdapter(context, getSupportFragmentManager());
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<TaskList> call = apiInterface.getTaskList(apiToken);
        call.enqueue(new Callback<TaskList>() {
            @Override
            public void onResponse(Call<TaskList> call, Response<TaskList> response) {
                taskList = response.body();
                if (response.isSuccessful()) {
                    Log.d("TAG", "status code: " + taskList.getStatusCode());
                    if (taskList.getStatusCode() == 200) {
                        /*if (taskList.getResponse().getConsignment() != null) {
                            saveConsignmentId(taskList.getResponse().getConsignment().getConsignmentUniqueId(), context);
                        } else {
                            saveConsignmentId("null", context);
                        }*/
                        mainActivityBinding.pager.setVisibility(View.VISIBLE);
                        mainActivityBinding.distanceLayout.setVisibility(View.VISIBLE);
                        mainActivityBinding.tabs.setVisibility(View.VISIBLE);
                        mainActivityBinding.emptyLayout.setVisibility(View.GONE);

                        dialogs.dismiss();

                        mainActivityBinding.tabs.setDistributeEvenly(true);
                        mainActivityBinding.tabs.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        mainActivityBinding.tabs.setSelectedBackgroundColors(getResources().getColor(R.color.main_root_header_color));
                        //mainActivityBinding.pager.setOffscreenPageLimit(2);
                        mainActivityBinding.pager.setAdapter(adapter);
                        mainActivityBinding.tabs.setViewPager(mainActivityBinding.pager);

                        mainActivityBinding.distanceTravelled.setText("Distance: " + "0.0" + " km");

                        Log.d("TAG", "task list size: " + taskList.getResponse().getTasks().size());
                        //if(taskList.getResponse().getTasks().size() > 0){
                        for (int i = 0; i < taskList.getResponse().getTasks().size(); i++) {
                            totalDistance = totalDistance + taskList.getResponse().getTasks().get(i).getDistance();
                            mainActivityBinding.distanceTravelled.setText("Distance: " + String.format("%.2f", totalDistance) + " km");

                            if (taskList.getResponse().getTasks().get(i).getTaskStatus().equals("Done")) {
                                doneTaskCounter = ++doneTaskCounter;
                            } else if (taskList.getResponse().getTasks().get(i).getTaskStatus().equals("Completed")) {
                                doneTaskCounter = ++doneTaskCounter;
                            }
                        }
                        //}

                        mainActivityBinding.distanceTask.setText(doneTaskCounter + "/" + taskList.getResponse().getTasks().size() + " task completed");
                        startRiderLocationService();

                    } else if (taskList.getStatusCode() == 204) {
                        dialogs.dismiss();
                        mainActivityBinding.pager.setVisibility(View.GONE);
                        mainActivityBinding.distanceLayout.setVisibility(View.GONE);
                        mainActivityBinding.tabs.setVisibility(View.GONE);
                        mainActivityBinding.emptyLayout.setVisibility(View.VISIBLE);
                        mainActivityBinding.emptyIcon.setText(taskList.getMessage().get(0).toString());
                    } else if (taskList.getStatusCode() == 403) {
                        Toast.makeText(context, "Please sign in again", Toast.LENGTH_LONG).show();
                        saveLoginStatus("0", context);
                        dialogs.dismiss();
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<TaskList> call, Throwable t) {
                Log.d("TAG", "Failed message: " + t.toString());
                dialogs.dismiss();
                mainActivityBinding.pager.setVisibility(View.GONE);
                mainActivityBinding.distanceLayout.setVisibility(View.GONE);
                mainActivityBinding.tabs.setVisibility(View.GONE);
                mainActivityBinding.emptyLayout.setVisibility(View.VISIBLE);
                mainActivityBinding.emptyIcon.setText("Please check your internet connection or try again.");
            }
        });
    }

    private void startRiderLocationService() {
        //startService(new Intent(this, RiderLocationService.class));
        //Calendar cur_cal = Calendar.getInstance();
        /*cur_cal.setTimeInMillis(System.currentTimeMillis());
        cur_cal.add(Calendar.SECOND, 50);*/
        //Log.d("Testing", "Calender Set time:" + cur_cal.getTime());

        //if (taskList.getResponse().getConsignment() != null) {
        /*Intent intent = new Intent(MainActivity.this, RiderLocationService.class);
        PendingIntent pintent = PendingIntent.getService(MainActivity.this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(), 60000, pintent);
        Toast.makeText(context, "Alarm Wake Up", Toast.LENGTH_SHORT).show();*/
        //}
        final Intent newIntent = new Intent(getApplicationContext(), RiderLocationService.class);
        newIntent.putExtra("get_consignment", "asif");
        final PendingIntent pending = PendingIntent.getService(getApplicationContext(), 0, newIntent, 0);
        final AlarmManager alarm = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pending);
        long interval = 60 * 1000 * 10;
        alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), interval, pending);

    }

    public String getApiToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        apiToken = preferences.getString("api_token", "null");
        Log.d("TAG", "Is LoginActivity status: " + apiToken);
        return apiToken;
    }

    public void progressD() {
        dialogs = new ProgressDialog(context);
        dialogs.setProgress(ProgressDialog.STYLE_SPINNER);
        dialogs.setMessage("Please wait...");
        dialogs.setCancelable(false);
        dialogs.show();
    }

    private void saveLoginStatus(String loginStatus, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("is_login", loginStatus);
        editor.commit();
    }
}
