package com.sslwireless.biddyut.rider.model.basic;

/**
 * Created by asif.malik on 8/29/2017.
 */

public class BasicModel {
    private int reason_id;
    private String remarks;

    public BasicModel() {

    }

    public BasicModel(int reason_id, String remarks) {
        this.reason_id = reason_id;
        this.remarks = remarks;
    }

    public int getReason_id() {
        return reason_id;
    }

    public void setReason_id(int reason_id) {
        this.reason_id = reason_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
